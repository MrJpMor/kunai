#version 330

uniform mat4 u_mvp;

in vec2 in_position;

void main(void)
{
	gl_Position = u_mvp * vec4(in_position, 0.0, 1.0);
}