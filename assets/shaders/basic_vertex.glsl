#version 330

uniform mat4 u_mvp;
//uniform float u_dist;

in vec2 in_position;
in vec3 in_color;
in vec2 in_uv;

out vec3 ex_Color;
out vec2 uv;

void main(void)
{
	gl_Position = u_mvp * vec4(in_position, 0.0, 1.0);
	ex_Color = in_color;
	uv = in_uv;
}