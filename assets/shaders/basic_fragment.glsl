#version 330

precision highp float; // needed only for version 1.30

uniform bool u_texture;
uniform sampler2D u_texture_sampler;

in vec3 ex_Color;
in vec2 uv;

out vec4 out_Color;

void main(void)
{
	if(u_texture)
	{
		out_Color = texture(u_texture_sampler, uv);
		out_Color = vec4(out_Color.a, out_Color.r, out_Color.g, out_Color.b);
	}
	else
	{
		out_Color = vec4(ex_Color,1.0);
	}
}