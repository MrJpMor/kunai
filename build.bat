@echo off

mkdir build\debug\
pushd build\debug\

del "*.exe"

..\..\tools\ctime -begin kunai_build_timings.ctm

cl -Zi /Fekunai_debug ..\..\src\main_win32.cpp user32.lib gdi32.lib opengl32.lib winmm.lib glew32.lib glu32.lib

..\..\tools\ctime -end kunai_build_timings.ctm
popd