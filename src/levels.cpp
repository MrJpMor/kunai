//
// Levels
//

internal void
GenerateLineSurfaceBuffer(LineSurface *line_surface)
{
	const u32 point_count = line_surface->point_count;

	const u32 control_count = line_surface->control_count;

	const u32 curve_count = line_surface->curve_count;

	const u32 render_point_count = point_count + (curve_count * CURVE_RESOLUTION);
	
	// TODO: Dont allocate if already done
	r32 *render_points = (r32*)PushStruct(&game_memory.permanent_memory, r32, render_point_count*2);
	u32 cur_render_point_count = 0;
	u32 cur_render_point_index = 0;

	for (u32 point_index = 0; point_index < line_surface->point_count; ++point_index)
	{
		u32 controls = 0;
		for(u32 ctrl_index = 0; ctrl_index < line_surface->control_count; ctrl_index++) // Iteracao pelos indices dos control points p/ ver se apresenta curva(e de que tipo).
		{
			if(line_surface->controls[ctrl_index] == point_index)
			{
				++controls;
				for (u32 i = ctrl_index+1; i < line_surface->control_count; ++i)
				{
					if((line_surface->controls[i] - line_surface->controls[ctrl_index]) == 1) ++controls;
					else break;
				}
				break;
			}
			else if(line_surface->controls[ctrl_index] > point_index) break;
		}

		switch(controls)
		{
			case 0: // Grau 1
			{
				render_points[cur_render_point_index++] = line_surface->points[point_index].x;
				render_points[cur_render_point_index++] = line_surface->points[point_index].y;
				cur_render_point_count++;
			}break;

			case 1: // Grau 2
			{
				if((point_index+1) < line_surface->point_count)
				{
					u32 added = GenerateQuadraticBezier2D(line_surface->points[point_index-1], line_surface->points[point_index], line_surface->points[point_index+1],
														CURVE_RESOLUTION, &render_points[cur_render_point_index]);
					cur_render_point_count += added;
					cur_render_point_index += (added*2);
				}
				else InvalidPath;
			}break;

			case 2: // Grau 3
			{
				if((point_index+2) < line_surface->point_count)
				{
					u32 added = GenerateCubicBezier2D(line_surface->points[point_index-1], line_surface->points[point_index], line_surface->points[point_index+1], line_surface->points[point_index+2],
														CURVE_RESOLUTION, &render_points[cur_render_point_index]);
					cur_render_point_count += added;
					cur_render_point_index += (added*2);
				}
				else InvalidPath;
			}break;

			default: InvalidPath; // corruption abounds among your data! panic!
		}

		point_index += clamp(0, 1, controls-1);
	}

	//if(render_point_count == cur_render_point_count)

	line_surface->buffer.gpu_id = 0;
	line_surface->buffer.points = &render_points[0];
	line_surface->buffer.point_count = cur_render_point_count;
	line_surface->buffer.color = V3(1.0f, 0.0f, 0.0f);
}

internal void
CreateRoundEdgedPlatform(GameState *game_state, r32 width, r32 height)
{
	LineSurface ls = {};
	ls.point_count = 9;
	ls.points = (V2*)PushStruct(&game_memory.permanent_memory, V2, ls.point_count);
	ls.points[0] = V2(1.5f*width, -0.5f*height);
	ls.points[1] = V2(-1.5f*width, -0.5f*height);
	ls.points[2] = V2(-2.5f*width, -0.5f*height); // control
	ls.points[3] = V2(-2.5f*width, 0.5f*height); // control
	ls.points[4] = V2(-1.5f*width, 0.5f*height);
	ls.points[5] = V2(1.5f*width, 0.5f*height);
	ls.points[6] = V2(2.5f*width, 0.5f*height); // control
	ls.points[7] = V2(2.5f*width, -0.5f*height); // control
	ls.points[8] = V2(1.5f*width, -0.5f*height);

	ls.control_count = 4;
	ls.controls = (u32*)PushStruct(&game_memory.permanent_memory, u32, ls.control_count);
	ls.controls[0] = 2;
	ls.controls[1] = 3;
	ls.controls[2] = 6;
	ls.controls[3] = 7;
	
	ls.curve_count = 2;

	GenerateLineSurfaceBuffer(&ls);

	game_state->line_surfaces[game_state->line_surface_count++] = ls;
}

internal void
CreateConvex(GameState *game_state)
{
	LineSurface ls = {};
	ls.point_count = 5;
	ls.points = (V2*)PushStruct(&game_memory.permanent_memory, V2, ls.point_count);
	ls.points[0] = V2(0.5f,-0.5f);
	ls.points[1] = V2(-0.5f,-0.5f);
	ls.points[2] = V2(-0.5f,0.5f);
	ls.points[3] = V2(0.5f,0.5f);
	ls.points[4] = V2(0.5f,-0.5f);

	ls.control_count = 1;
	ls.controls = (u32*)PushStruct(&game_memory.permanent_memory, u32, ls.control_count);
	ls.controls[0] = 2;
	
	ls.curve_count = 1;

	GenerateLineSurfaceBuffer(&ls);

	game_state->line_surfaces[game_state->line_surface_count++] = ls;
}

internal void
CreateConcave(GameState *game_state)
{
	LineSurface ls = {};
	ls.point_count = 5;
	ls.points = (V2*)PushStruct(&game_memory.permanent_memory, V2, ls.point_count);
	ls.points[0] = V2(0.5f,-0.5f);
	ls.points[1] = V2(-0.5f,-0.5f);
	ls.points[2] = V2(0.5f,-0.5f);
	ls.points[3] = V2(0.5f,0.5f);
	ls.points[4] = V2(0.5f,-0.5f);

	ls.control_count = 1;
	ls.controls = (u32*)PushStruct(&game_memory.permanent_memory, u32, ls.control_count);
	ls.controls[0] = 2;
	
	ls.curve_count = 1;

	GenerateLineSurfaceBuffer(&ls);

	game_state->line_surfaces[game_state->line_surface_count++] = ls;
}

internal void
GenerateLineSurfaces(GameState *game_state)
{
	//CreateRoundEdgedPlatform(game_state, 2.0f);
	//CreateRoundEdgedPlatform(game_state, 5.0f, 1.0f);
	//CreateRoundEdgedPlatform(game_state, 0.5f);
	//CreateConvex(game_state);
	CreateConcave(game_state);
}
