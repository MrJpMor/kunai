#include <gl\glew.h>
#include <gl\wglew.h>
#pragma comment(lib, "glew32.lib")

internal GLuint
OpenGLCompileShader(const char *filename, GLenum type)
{
	GLuint shader = glCreateShader(type);

	const GLchar *source = (const GLchar *)PlatformGetTextFileContents(filename);
	glShaderSource(shader, 1, &source, 0);

	glCompileShader(shader);

	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if(isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		char *log = (char*)calloc(1, maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, (GLchar*)log);
		
		glDeleteShader(shader);

		PlatformShowPopupTextWindow(log);

		return 0;
	}

	return shader;
}

internal GLuint
OpenGLCreateShaderProgram(GLuint vertex_shader, GLuint fragment_shader)
{
	GLuint program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	return program;
}

internal void
OpenGLLinkShaderProgram(GLuint program)
{
	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if(isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		char *log = (char*)calloc(1, maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, (GLchar*)log);
		
		glDeleteProgram(program);

		PlatformShowPopupTextWindow(log);
		
		return;
	}
}

#define A_POSITION "in_position"
#define A_COLOR "in_color"
#define A_UV "in_uv"
#define U_TEXTURE_SAMPLER "u_texture_sampler"
#define U_TEXTURE "u_texture"
#define U_COLOR "u_color"
#define U_DIST "u_dist"
#define U_MVP "u_mvp"

struct BasicShader
{
	GLuint u_dist;
	GLuint u_mvp_location;
	GLuint u_texture_location;
	GLuint u_texture_sampler_location;

	GLuint a_position;
	GLuint a_color;
	GLuint a_uv;

	GLuint id;
};
global_variable BasicShader basic_shader;

internal void
InitBasicShader()
{
	GLuint vertex_shader = OpenGLCompileShader("shaders\\basic_vertex.glsl", GL_VERTEX_SHADER);
	GLuint fragment_shader = OpenGLCompileShader("shaders\\basic_fragment.glsl", GL_FRAGMENT_SHADER);
	basic_shader.id = OpenGLCreateShaderProgram(vertex_shader, fragment_shader);
	
	basic_shader.a_position = 0;
	glBindAttribLocation(basic_shader.id, basic_shader.a_position, A_POSITION);
	basic_shader.a_color = 1;
	glBindAttribLocation(basic_shader.id, basic_shader.a_color, A_COLOR);
	basic_shader.a_uv = 2;
	glBindAttribLocation(basic_shader.id, basic_shader.a_uv, A_UV);
	
	OpenGLLinkShaderProgram(basic_shader.id);
	glDetachShader(basic_shader.id, vertex_shader);
	glDetachShader(basic_shader.id, fragment_shader);
	glUseProgram(basic_shader.id);
	//basic_shader.u_dist = glGetUniformLocation(basic_shader.id, U_DIST);
	basic_shader.u_mvp_location = glGetUniformLocation(basic_shader.id, U_MVP);
	basic_shader.u_texture_location = glGetUniformLocation(basic_shader.id, U_TEXTURE);
	basic_shader.u_texture_sampler_location = glGetUniformLocation(basic_shader.id, U_TEXTURE_SAMPLER);
	glUseProgram(0);
}

struct Line2DShader
{
	GLuint u_dist;
	GLuint u_mvp_location;
	GLuint u_color_location;

	GLuint a_position;

	GLuint id;
};
global_variable Line2DShader line2D_shader;

internal void
InitLine2DShader()
{
	GLuint vertex_shader = OpenGLCompileShader("shaders\\2Dline_vertex.glsl", GL_VERTEX_SHADER);
	GLuint fragment_shader = OpenGLCompileShader("shaders\\2Dline_fragment.glsl", GL_FRAGMENT_SHADER);
	line2D_shader.id = OpenGLCreateShaderProgram(vertex_shader, fragment_shader);
	
	line2D_shader.a_position = 0;
	glBindAttribLocation(line2D_shader.id, line2D_shader.a_position, A_POSITION);
	
	OpenGLLinkShaderProgram(line2D_shader.id);
	glDetachShader(line2D_shader.id, vertex_shader);
	glDetachShader(line2D_shader.id, fragment_shader);
	glUseProgram(line2D_shader.id);
	//line2D_shader.u_dist = glGetUniformLocation(line2D_shader.id, U_DIST);
	line2D_shader.u_mvp_location = glGetUniformLocation(line2D_shader.id, U_MVP);
	line2D_shader.u_color_location = glGetUniformLocation(line2D_shader.id, U_COLOR);
	glUseProgram(0);
}


internal void
LineBufferToGPU (u32 *vao, r32 *data, u32 point_count)
{
	u32 vbo;
	if(!(*vao))
	{
		glGenVertexArrays(1, vao);
		glGenBuffers(1, &vbo);
	}
	
	glBindVertexArray(*vao);
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, point_count*2*sizeof(GLfloat), (GLfloat*)data, GL_STATIC_DRAW);
	glVertexAttribPointer(line2D_shader.a_position, 2, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(line2D_shader.a_position);

	glBindVertexArray(0);
}

global_variable GLuint rect_vao;
global_variable GLuint rect_xy_vbo;
global_variable GLuint rect_uv_vbo;

global_variable GLuint grid_vao;
global_variable GLuint grid_vbo;
global_variable u32 grid_point_count;

//global_variable V3 cubic_bezier[] = {V3(0.0f, 0.0f, 0.0f), V3(-1.0f, -2.0f, 0.0f), V3(5.0f, -2.0f, 0.0f), V3(0.0f, 0.0f, -1.0f)};
//global_variable V3 cubic_bezier_lines[BEZIER_RESOLUTION];

internal void
OpenGLInit(HDC hdc)
{
	PIXELFORMATDESCRIPTOR desired_pf = {};
	desired_pf.nSize = sizeof(desired_pf);
	desired_pf.nVersion = 1;
	desired_pf.dwFlags = PFD_SUPPORT_OPENGL|PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER;
	desired_pf.cColorBits = 32;
	desired_pf.cAlphaBits = 32;
	desired_pf.iLayerType = PFD_MAIN_PLANE;
	
	i32 suggested_pf_index = ChoosePixelFormat(hdc, &desired_pf);
	PIXELFORMATDESCRIPTOR suggested_pf = {};
	DescribePixelFormat(hdc, suggested_pf_index, sizeof(suggested_pf), &suggested_pf);
	SetPixelFormat(hdc, suggested_pf_index, &suggested_pf);

	HGLRC glrc;
	HGLRC temp_glrc = wglCreateContext(hdc);
	if(temp_glrc && wglMakeCurrent(hdc, temp_glrc))
	{	
		// Inicializacao do contexto com GLEW
		// @Temp: Nao usar bibliotecas p/ extensoes (carregar funcoes individualmente)

		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			PlatformShowPopupTextWindow("GLEW init Failed!");
		}

		int attribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 2,
			WGL_CONTEXT_FLAGS_ARB, 0,
			0
		};
		
        if(wglewIsSupported("WGL_ARB_create_context") == 1)
        {
			glrc = wglCreateContextAttribsARB(hdc,0,attribs);
			wglMakeCurrent(NULL,NULL);
			wglDeleteContext(temp_glrc);
			wglMakeCurrent(hdc, glrc);
		}
		else
		{	// N foi possivel criar contexto 3.x, criar 2.1
			glrc = temp_glrc;
		}

		glGetIntegerv(GL_MAJOR_VERSION, (GLint*)&gl_version_major);
		glGetIntegerv(GL_MINOR_VERSION, (GLint*)&gl_version_minor);

	}
	else 
	{
		assert(0);
	}

	// Inicializando
	InitBasicShader();
	InitLine2DShader();

	GLuint vaos[1];
	glGenVertexArrays(1, vaos);
	rect_vao = vaos[0];

	GLuint vbos[2];
	glGenBuffers(2, vbos);
	rect_xy_vbo = vbos[0];
	rect_uv_vbo = vbos[1];
	
	glBindVertexArray(rect_vao);

	glBindBuffer(GL_ARRAY_BUFFER, rect_xy_vbo);
	glBufferData(GL_ARRAY_BUFFER, 6*2*sizeof(GLfloat), rect_xy, GL_STATIC_DRAW);
	glVertexAttribPointer(basic_shader.a_position, 2, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(basic_shader.a_position);

	glBindBuffer(GL_ARRAY_BUFFER, rect_uv_vbo);
	glBufferData(GL_ARRAY_BUFFER, 6*2*sizeof(GLfloat), rect_uv, GL_STATIC_DRAW);
	glVertexAttribPointer(basic_shader.a_uv, 2, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(basic_shader.a_uv);
	
	glBindVertexArray(0);

	glFrontFace(GL_CW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);

	glLineWidth(1.5f);
	glPointSize(3.0f);

	/*
	*/
}

internal void
OpenGLResize(RenderInfo *info, u32 width, u32 height)
{
	info->window_width = width;
	info->window_height = height;
	glViewport(0, 0, width, height);
}

internal void 
BeginRendering(RenderInfo *info)
{
	glClearColor(info->clear_color.x, info->clear_color.y, info->clear_color.z, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

internal void 
RenderRect(Mat4 mvp, Rect rect)
{
	glUseProgram(basic_shader.id);
	glBindVertexArray(rect_vao);
	
	GLfloat mvp_vals[] = Mat4ValueArray(mvp);
	glUniformMatrix4fv(basic_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
	glUniform1i(basic_shader.u_texture_location, rect.fill);
	//glUniform1f(basic_shader.u_dist, (GLfloat)GROUND_Z);
	glVertexAttrib3f((GLuint)basic_shader.a_color, rect.color.x, rect.color.y, rect.color.z);

	
	if(rect.fill)
	{
		glUniform1i(basic_shader.u_texture_sampler_location, 0);
		glBindTexture(GL_TEXTURE_2D, rect.texture_id);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	if(rect.outline)
	{
		ApplyScale(&mvp, V2(1.1f, 1.1f));
		GLfloat mvp_vals_o[] = Mat4ValueArray(mvp);
		glUniformMatrix4fv(basic_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals_o[0]);
		
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_SHORT, rect_outline_indices);
	}

	glBindVertexArray(0);
	glUseProgram(0);
}

internal void 
RenderLines(Mat4 mvp, LineBuffer *buffer)
{
	glUseProgram(line2D_shader.id);

	GLfloat mvp_vals[] = Mat4ValueArray(mvp);
	glUniformMatrix4fv(line2D_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
	glUniform3f(line2D_shader.u_color_location, (GLfloat)buffer->color.x,(GLfloat)buffer->color.y,(GLfloat)buffer->color.z);
	//glUniform1f(line2D_shader.u_dist, (GLfloat)GROUND_Z);

	if(!buffer->gpu_id)
	{
		if(buffer->points) LineBufferToGPU (&buffer->gpu_id, buffer->points, buffer->point_count);
		else assert(0); // the buffer is shit
	}

	glBindVertexArray(buffer->gpu_id);
	glDrawArrays(GL_LINES, 0, buffer->point_count);
	
	glBindVertexArray(0);
	glUseProgram(0);
}

internal void 
RenderLineStrip(Mat4 mvp, LineBuffer *buffer)
{
	glUseProgram(line2D_shader.id);

	GLfloat mvp_vals[] = Mat4ValueArray(mvp);
	glUniformMatrix4fv(line2D_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
	glUniform3f(line2D_shader.u_color_location, (GLfloat)buffer->color.x,(GLfloat)buffer->color.y,(GLfloat)buffer->color.z);
	//glUniform1f(line2D_shader.u_dist, (GLfloat)GROUND_Z);

	if(!buffer->gpu_id)
	{
		if(buffer->points) LineBufferToGPU (&buffer->gpu_id, buffer->points, buffer->point_count);
		else assert(0); // the buffer is shit
	}

	glBindVertexArray(buffer->gpu_id);
	glDrawArrays(GL_LINE_STRIP, 0, buffer->point_count);
	
	glBindVertexArray(0);
	glUseProgram(0);
}


internal u32
LoadBMP(const char *filename)
{
	u32 res = 0;
	u8 *contents = (u8*)PlatformGetFileContents(filename);
	if(contents)
	{
		if(contents[0] != 'B' || contents[1] != 'M') assert(0);

		u32 data_pos = *(u32*)&(contents[0x0A]);
		if(data_pos==0) data_pos = 54;
		u8 *data = contents + data_pos;

		u32 width = *(u32*)&(contents[0x12]);
		u32 height = *(u32*)&(contents[0x16]);
		u32 image_size = *(u32*)&(contents[0x22]);
		if(image_size==0) image_size = width*height*4;

		glGenTextures(1, &res);

		glBindTexture(GL_TEXTURE_2D, res);
		glTexImage2D(GL_TEXTURE_2D, 0,GL_BGRA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}

	return res;
}

/*
internal void
OpenGLRender(RenderBuffer *render_buffer)
{
	glClearColor(0.14901960784f, 0.19607843137f, 0.21960784313f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Mat4 model_matrix = Mat4Identity;

	Mat4 view_matrix = Mat4Identity;
	SetLookAt (&view_matrix, render_buffer->camera_p.x, render_buffer->camera_p.y, render_buffer->camera_d,  
							 render_buffer->camera_p.x, render_buffer->camera_p.y, -1.0f,
							 0.0f, 1.0f, 0.0f);

	Mat4 projection_matrix = Mat4Identity;
	r32 ratio = (r32) render_buffer->width / (r32)render_buffer->height;
	//SetOrthographicProjection (&projection_matrix, -ratio, ratio, -1.0f, 1.0f, 0.01f, 100.0f);
	//SetFrustum (&projection_matrix, -ratio, ratio, -1.0f, 1.0f, 0.1f, 1.0f);
	SetPerspectiveProjection (&projection_matrix, 0.01f, 100.0f, 90.0f, ratio);

	Mat4 mvp_matrix = Mat4Identity;


	glUseProgram(line2D_shader.id);

	model_matrix = Mat4Identity;
	mvp_matrix = (model_matrix * view_matrix) * projection_matrix;
	GLfloat mvp_vals[] = Mat4ValueArray(mvp_matrix);
	glUniformMatrix4fv(line2D_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
	glUniform3f(line2D_shader.u_color_location, (GLfloat)1.0f,(GLfloat)0.0f,(GLfloat)0.0f);
	glUniform1f(line2D_shader.u_dist, (GLfloat)GROUND_Z);

	// line strips de vizualizacao dos LineSurfaces
	for(u32 line_strip_index = 0;
		line_strip_index < render_buffer->line_strip_count;
		line_strip_index++)
	{
		if(render_buffer->line_strips[line_strip_index]) // posicoes presentes
		{
			if(!render_buffer->line_strip_ids_0[line_strip_index]) // mandar p/ gpu se necessario
				LineStripToGPU (&render_buffer->line_strip_ids_0[line_strip_index], &render_buffer->line_strip_ids_1[line_strip_index],
								 render_buffer->line_strips[line_strip_index], render_buffer->line_strip_point_counts[line_strip_index]);

			glBindVertexArray(render_buffer->line_strip_ids_0[line_strip_index]);

			glDrawArrays(GL_LINE_STRIP, 0, render_buffer->line_strip_point_counts[line_strip_index]);
		}
	}

	//grid
	if(!grid_vao) // mandar p/ gpu
	{
		r32 grid_res = 0.5f;
		r32 grid_span = 16.0f;
		r32 limit = grid_span/2.0f;
		r32 right_limit = limit;
		r32 left_limit = -limit;
		r32 top_limit = limit;
		r32 bottom_limit = -limit;

		grid_point_count = (u32)grid_span*grid_span;
		u32 grid_point_xys_size = grid_point_count*2;//(2*2*2*grid_span)/grid_res;
		r32 *grid_point_xys = (r32*)PushStruct(&game_memory.transient_memory, r32, grid_point_xys_size);
		u32 xyi = 0;
		for (r32 x = left_limit;
			 x <= right_limit;
			 x += grid_res)
		{
			grid_point_xys[xyi++] = x;
			grid_point_xys[xyi++] = bottom_limit;
			grid_point_xys[xyi++] = x;
			grid_point_xys[xyi++] = top_limit;
		}
		for (r32 y = bottom_limit;
			 y <= top_limit;
			  y += grid_res)
		{
			grid_point_xys[xyi++] = left_limit;
			grid_point_xys[xyi++] = y;
			grid_point_xys[xyi++] = right_limit;
			grid_point_xys[xyi++] = y;
		}
	
		glGenVertexArrays(1, &grid_vao);
		glGenBuffers(1, &grid_vbo);

		glBindVertexArray(grid_vao);
		glBindBuffer(GL_ARRAY_BUFFER, grid_vbo);
		glBufferData(GL_ARRAY_BUFFER, grid_point_xys_size*sizeof(GLfloat), grid_point_xys, GL_STATIC_DRAW);
		glVertexAttribPointer(line2D_shader.a_position, 2, GL_FLOAT, GL_FALSE, 0, 0); 
		glEnableVertexAttribArray(line2D_shader.a_position);
		glBindVertexArray(0);
	}

	glBindVertexArray(grid_vao);
	glUniformMatrix4fv(line2D_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
	glUniform3f(line2D_shader.u_color_location, (GLfloat)0.27058823529f,(GLfloat)0.35294117647f,(GLfloat)0.39215686274f);
	glUniform1f(line2D_shader.u_dist, (GLfloat)GRID_Z);
	glDrawArrays(GL_LINES, 0, grid_point_count);


	glUseProgram(basic_shader.id);
	glBindVertexArray(rect_vao);
	for(u32 rect_index = 0;
		rect_index < render_buffer->rect_count;
		rect_index++)
	{
		Rect r = render_buffer->rects[rect_index];

		ApplyTransforms(&model_matrix, r.p, r.s, r.r);
		mvp_matrix = (model_matrix * view_matrix) * projection_matrix;

		GLfloat mvp_vals[] = Mat4ValueArray(mvp_matrix);
		glUniformMatrix4fv(basic_shader.u_mvp_location, 1, FALSE, (GLfloat*)&mvp_vals[0]);
		glUniform1i(basic_shader.u_texture_location, r.fill);
		glUniform1f(basic_shader.u_dist, (GLfloat)GROUND_Z);
		glVertexAttrib3f((GLuint)basic_shader.a_color, r.color.x, r.color.y, r.color.z);

		
		if(r.fill)
		{
			glUniform1i(basic_shader.u_texture_sampler_location, 0);
			glBindTexture(GL_TEXTURE_2D, r.texture_id);
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}

		if(r.outline)
		{
			glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_SHORT, rect_outline_indices);
		}

	}
	glBindVertexArray(0);
	glUseProgram(0);
}
*/