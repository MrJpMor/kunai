/*
	TASKS:
		- pop struct?(desalocar memoria)
		- alocacao de tamanho dinamico
*/

#define KILOBYTES(n) (n*1024)
#define MEGABYTES(n) (KILOBYTES(n)*1024)
#define GIGABYTES(n) (MEGABYTES(n)*1024)

struct MemoryBlock
{
	u32 size;
	u32 used;
	u8 *base;
};

struct GameMemory
{
	MemoryBlock permanent_memory;
	MemoryBlock transient_memory;
};
global_variable GameMemory game_memory;

internal void
InitializeMemoryFromBase (MemoryBlock *memory, u8 *base, u32 size)
{
	memory->base = base;
	memory->size = size;
	memory->used = 0;
}

internal void
InitializeMemory (MemoryBlock *memory, u32 size)
{
	memory->base = (u8*)PlatformAlloc(size);
	memory->size = size;
	memory->used = 0;
}

internal void*
PushStruct_ (MemoryBlock *memory, u32 size)
{
	assert((memory->used + size) <= memory->size);
	void *res = memory->base + memory->used;
	memory->used += size;
	return res;
}
#define PushStruct(memory, type, count) ((type*) PushStruct_((memory), sizeof(type) * (count)))