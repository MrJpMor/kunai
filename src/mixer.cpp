
//
//	WAV loader
//

#pragma pack(push, 1)
struct WaveHeader
{
	u32 RIFFID;
	u32 size;
	u32 WAVEID;
};

#define RIFF_CODE(a, b, c, d) (((u32)(a) << 0) | ((u32)(b) << 8) | ((u32)(c) << 16) | ((u32)(d) << 24))
enum
{
	WAVE_CHUNKID_fmt  = RIFF_CODE('f','m','t',' '),
	WAVE_CHUNKID_RIFF = RIFF_CODE('R','I','F','F'),
	WAVE_CHUNKID_WAVE = RIFF_CODE('W','A','V','E'),
	WAVE_CHUNKID_data = RIFF_CODE('d','a','t','a')
};

struct WaveChunk
{
	u32 id;
	i32 size;	
};

struct WaveFmt
{
	u16 w_format_tag;
	u16 n_chanels;
	u32 n_samples_per_sec;
	u32 n_avg_bytes_per_sec;
	u16 n_block_align;
	u16 w_bits_per_sample;
	u32 dw_channel_mask;
	u8  sub_format[16];	
};
#pragma pack(pop)

struct RiffIterator
{
	u8 *at;
	u8 *stop;
};

inline RiffIterator
ParseChunkAt(void *at, void *stop)
{
	RiffIterator it;
	it.at = (u8*)at;
	it.stop = (u8*)stop;
	return it;
}

inline RiffIterator
NextChunk(RiffIterator it)
{
	WaveChunk *chunk = (WaveChunk*)it.at;
	u32 size = (chunk->size + 1) & ~1;
	it.at += sizeof(WaveChunk) + size;
	return it;
}

inline b32
IsValid(RiffIterator it)
{
	return (it.at < it.stop);
}

inline void *
GetChunkData(RiffIterator it)
{
	return (void*) (it.at + sizeof(WaveChunk));
}

inline u32
GetChunkDataSize(RiffIterator it)
{
	WaveChunk *chunk = (WaveChunk*)it.at;
	return chunk->size;
}

inline u32
GetType(RiffIterator it)
{
	WaveChunk *chunk = (WaveChunk*)it.at;
	return (chunk->id);
}

internal Sound
LoadWAV (char *filename)
{
	Sound res = {};
	void *contents = PlatformGetFileContents(filename);
	if(contents)
	{
		WaveHeader *header = (WaveHeader*)contents;
		assert(header->RIFFID == WAVE_CHUNKID_RIFF);
		assert(header->WAVEID == WAVE_CHUNKID_WAVE);

		u32 channel_count = 0;
		i16 *sample_data = 0;
		u32 sample_data_size = 0;

		for(RiffIterator it = ParseChunkAt((header + 1), (u8*)(header + 1) + header->size-4);
			IsValid(it);
			it = NextChunk(it))
		{
			switch(GetType(it))
			{
				case WAVE_CHUNKID_fmt:
				{
					WaveFmt *fmt = (WaveFmt*)GetChunkData(it);
					
					assert(fmt->w_format_tag == 1); // PCM
					assert(fmt->n_samples_per_sec == 48000);
					assert(fmt->w_bits_per_sample == 16);
					assert(fmt->n_block_align == sizeof(i16)*fmt->n_chanels);

					channel_count = fmt->n_chanels;
				} break;

				case WAVE_CHUNKID_data:
				{
					sample_data = (i16*)GetChunkData(it);
					sample_data_size = GetChunkDataSize(it);
				} break;
			}
		}

		assert(channel_count && sample_data);

		res.channel_count = channel_count;
		res.sample_count = sample_data_size / (channel_count * sizeof(i16));
		if(channel_count == 1)
		{
			res.samples[0] = sample_data;
			res.samples[1] = 0;
		}
		else if(channel_count == 2)
		{
			res.samples[0] = sample_data;
			res.samples[1] = sample_data + res.sample_count;
			
			/*
			for(u32 sample_index = 0;
				sample_index < res.sample_count;
				++sample_index)
			{
				sample_data[2*sample_index + 0] = (u16)sample_index;
				sample_data[2*sample_index + 1] = (u16)sample_index;
			}*/

			for(u32 sample_index = 0;
				sample_index < res.sample_count;
				++sample_index)
			{
				i16 src = sample_data[2*sample_index];
				sample_data[2*sample_index] = sample_data[sample_index];
				sample_data[sample_index] = src;
			}

		}
		else
		{
			PlatformShowPopupTextWindow("# invalido de channels no WAV!");
		}

		//TODO: load right
		res.channel_count = 1;
	}
	return res;
}

//
// Mixer
//

#define SAMPLES_PER_SEC 48000
#define TONE_HZ 100
#define WAVE_PERIOD (SAMPLES_PER_SEC/TONE_HZ)
#define TONE_VOLUME 0

struct GameSoundOutputBuffer
{
	i16 *samples;
	u32 sample_count_to_out;
};

internal void
MixerInit (GameSoundOutputBuffer *sound_out)
{
	sound_out->sample_count_to_out = (SAMPLES_PER_SEC / TARGET_FPS);
	sound_out->samples = (i16*)PushStruct(&game_memory.permanent_memory, i16, sound_out->sample_count_to_out);
}

internal void
PlaySoundEffect()
{

}

internal void
MixerOutputSound(GameSoundOutputBuffer *sound_out)
{
	GameState *game_state = (GameState*)game_memory.permanent_memory.base;

	local_persist r32 tsine;

	i16 *sample_out = sound_out->samples;
	i16 *sample_src = game_state->ambient_sound.samples[0];
	for(u32 sample_index = 0;
		sample_index < sound_out->sample_count_to_out;
		++sample_index)
	{
		r32 sinev = sinf(tsine);
		i16 sample_value = (i16)(sinev * TONE_VOLUME);
		*sample_out++ = sample_value;//*sample_src++;
		*sample_out++ = sample_value;//*sample_src++;

		tsine += 2.0f*PI*(1.0f/WAVE_PERIOD);
	}
}