//
// Renderer
//

global_variable r32 rect_xy[] =
{
	-0.5f, -0.5f,
	0.5f, 0.5f, 
	0.5f, -0.5f,

	-0.5f, -0.5f,
	-0.5f, 0.5f, 
	0.5f, 0.5f
};
global_variable r32 rect_uv[] = 
{
	0.0f,0.0f,
	1.0f,1.0f,
	1.0f,0.0f,

	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f
};
global_variable u16 rect_outline_indices[] = 
{
	0, 4, 1, 2
};

#define CURVE_RESOLUTION 24

#define GRID_Z -0.001f
#define GROUND_Z 0.001f
#define FOREGROUND_Z 0.0001f
#define BACKGROUND_Z -0.1f

struct Rect
{
	b32 fill;
	b32 outline;
	u32 texture_id;
	V3 color;
};

/*
	u32 line_strip_ids_0 [MAX_LINE_STRIPS];
	u32 line_strip_ids_1 [MAX_LINE_STRIPS];
	r32 *line_strips [MAX_LINE_STRIPS];
	u32 line_strip_point_counts [MAX_LINE_STRIPS];
	u32 line_strip_count;

	r32 *grid_line_points;
	u32 grid_line_point_count;
*/

struct RenderInfo
{
	u32 window_width;
	u32 window_height;

	V3 clear_color;
} render_info;

internal u32 LoadBMP(const char *filename);

internal void BeginRendering(RenderInfo *info);

internal void RenderRect(Mat4 mvp, Rect rect);
internal void RenderLines(Mat4 mvp, LineBuffer *buffer);
internal void RenderLineStrip(Mat4 mvp, LineBuffer *buffer);

//internal void EndRendering();