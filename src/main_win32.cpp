#include <assert.h>
//#include <stdio.h>
//#include <io.h>
//#include <fcntl.h>

#define WINDOW_WIDTH 1080
#define WINDOW_HEIGHT 720

#include "basics.h"
#include "platform.h"
#include "memory.cpp"
#include "math.cpp"


global_variable b32 running_game; // @Temp: Manter global?
global_variable i32 gl_version_major;
global_variable i32 gl_version_minor;

#include <windows.h> // TODO: GET RID OF THIS FUCKER

#include "kunai.cpp"
#include "renderer_opengl.cpp"
#include "mixer_dsound.cpp"

LRESULT CALLBACK 
WindowProcedure(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			Input *input = (Input*) &((GameState*)game_memory.permanent_memory.base)->input;

			u32 vkcode = wparam;
			b32 isdown = ((lparam & (1<<31)) == 0);
			b32 wasdown = ((lparam & (1<<30)) != 0);
			b32 alt_down = ((lparam & (1<<29)) != 0);

			//if(alt_down) input->camera_ctrl = true;

			//if(wasdown != isdown)
			{
				switch(vkcode)
				{
					case VK_UP:
						//input->up = true;
					break;
					case VK_DOWN:
						//input->down = true;
					break;
					case VK_LEFT:
						//input->left = true;
					break;
					case VK_RIGHT:
						//input->right = true;
					break;

					case VK_SPACE:
					break;
		
					case 'W': 
					break;
					case 'S':
					break;
					case 'A':
					break;
					case 'D':
					break;

					case 'R':
						//if(isdown && !wasdown) input->reset = true;
					break;

					case 'G':
						//if(isdown && !wasdown) SHOW_GRID = !SHOW_GRID;
					break;

					case VK_F4:
						if(alt_down) running_game = false;						
					break;
				}
			}
		} break;
		
		case WM_MOUSEWHEEL: break;
		case WM_LBUTTONUP: break;
		case WM_ACTIVATEAPP: break;
		
		case WM_SIZE:
		{	
			RECT rect;
			GetWindowRect(window, &rect);
			u32 width = rect.right - rect.left;
			u32 height = rect.bottom - rect.top;
			OpenGLResize(&render_info, width, height);
		} break;
		
		case WM_CLOSE:
        case WM_DESTROY:
			running_game = false;
			break;

        default:
    		return DefWindowProc(window, msg, wparam, lparam);
    }
    return 0;
}

i32 WINAPI
WinMain (HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd, i32 cmd_show)
{
	//
	// Inicializacao da janela
	//

	const char szClassName[] = "KunaiWindowClass";
    WNDCLASSEX wc;
    HWND game_window;

    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wc.lpfnWndProc   = WindowProcedure;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = instance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = szClassName;
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }

    game_window = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        szClassName,
        GAME_TITLE,
        WS_OVERLAPPEDWINDOW|CS_OWNDC,
        CW_USEDEFAULT, CW_USEDEFAULT, 
		WINDOW_WIDTH, WINDOW_HEIGHT,
        NULL, NULL, instance, NULL);
    if(game_window == NULL)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }
    HDC hdc = GetDC(game_window);

    /*
    // Abrindo console
    AllocConsole();
	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;
    */

	InitializeMemory (&game_memory.permanent_memory, MEGABYTES(100));
	InitializeMemory (&game_memory.transient_memory, MEGABYTES(30));

    GameState *game_state = (GameState*)PushStruct(&game_memory.permanent_memory, GameState, 1);
    //RenderBuffer *render_buffer = (RenderBuffer*)PushStruct(&game_memory.permanent_memory, RenderBuffer, 1);
    //RendererInit(render_buffer);

	OpenGLInit(hdc);
    //char str[20];
	//wsprintf(str, "%s (GL %d.%d)", GAME_TITLE, gl_version_major,gl_version_minor);
	//SetWindowText(game_window, str);

    GameSoundOutputBuffer game_sound_buffer = {};
    MixerInit(&game_sound_buffer);
    DSoundOutput dsound_out = {};
    DSoundInit(game_window, &dsound_out, &game_sound_buffer);

    ShowWindow(game_window, cmd_show);

    //
    // Loop Principal
    //

	timeBeginPeriod(1);
    LARGE_INTEGER perf_frequency_i;
	QueryPerformanceFrequency(&perf_frequency_i);
	i64 perf_frequency = perf_frequency_i.QuadPart;
	r64 ms_since_last_s = 0.0;
	u32 frame_count = 0;
	i64 frame_start = PlatformGetTime();

    running_game = true;
    while(running_game)
    {
    	// Gerenciando mensagens do sistema
    	MSG msg;
		BOOL res;
		while((res = PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// Input
		#define WIN32_KEY_DOWN 0b1000000000000000
		// is this even ok?
		game_state->input.cam_up = INPUT_KEY(game_state->input.cam_up, (((GetKeyState(VK_UP) & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.cam_down = INPUT_KEY(game_state->input.cam_down, (((GetKeyState(VK_DOWN) & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.cam_left = INPUT_KEY(game_state->input.cam_left, (((GetKeyState(VK_LEFT) & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.cam_right = INPUT_KEY(game_state->input.cam_right, (((GetKeyState(VK_RIGHT) & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.cam_dist = INPUT_KEY(game_state->input.cam_dist, (((GetKeyState(VK_LCONTROL) & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.grid = INPUT_KEY(game_state->input.grid, (((GetKeyState('G') & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.up = INPUT_KEY(game_state->input.up, (((GetKeyState('W') & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.down = INPUT_KEY(game_state->input.down, (((GetKeyState('S') & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.left = INPUT_KEY(game_state->input.left, (((GetKeyState('A') & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));
		game_state->input.right = INPUT_KEY(game_state->input.right, (((GetKeyState('D') & WIN32_KEY_DOWN) == WIN32_KEY_DOWN) ? INPUT_DOWN : 0));

		// Update
		switch(game_state->mode)
		{
			case GAME_MODE_MENU: MenuUpdateAndRender();
			case GAME_MODE_PLAY: GameUpdateAndRender(&render_info);
		}

		// Render
		SwapBuffers(hdc);
		UpdateWindow(game_window);

		// Output de som
		DSoundOutputSound(&dsound_out, &game_sound_buffer);

		// Contando frames
		++frame_count;
		i64 frame_end = PlatformGetTime();
		r64 ms_for_frame = PlatformTimeElapsed(frame_start,frame_end, perf_frequency);
		game_state->input.dt = ((r32)ms_for_frame/1000.0f);
		
		if(ms_for_frame < TARGET_MS_PER_FRAME)
		{
			DWORD sleepms = TARGET_MS_PER_FRAME - ms_for_frame;
			Sleep(sleepms);
			ms_for_frame = PlatformTimeElapsed(frame_start, PlatformGetTime(), perf_frequency);
		}
		
		ms_since_last_s += ms_for_frame;
		if(ms_since_last_s >= 1000.0)
		{		
			char str[20];
			wsprintf(str, "%s [%d FPS]", GAME_TITLE, frame_count, game_state->input.dt);
			SetWindowText(game_window, str);
			
			ms_since_last_s = 0.0;
			frame_count = 0;
		}

		frame_start = frame_end;
    }

	ReleaseDC(game_window,hdc);
    return 0;
}

//
// Files
//

internal void *
PlatformGetFileContents(const char *filename)
{
	void *contents = 0;
	
	HANDLE file_handle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if(file_handle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER file_size;
		if(GetFileSizeEx(file_handle, &file_size))
		{
			contents = VirtualAlloc(0, (u32)file_size.QuadPart, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if(contents)
			{
				DWORD bytes_read;
				if(ReadFile(file_handle, contents, (u32)file_size.QuadPart, &bytes_read, 0))
				{
					// 
				}
				else
				{
					MessageBox(NULL, "Failed to read file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
					VirtualFree(contents, 0, MEM_RELEASE);
				}
			}
			else
			{
				MessageBox(NULL, "Failed to alloc mem for file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
			}
		}

		CloseHandle(file_handle);
	}
	else
	{
		MessageBox(NULL, "Failed to open file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
	}

	return contents;
}

internal char *
PlatformGetTextFileContents (const char *filename)
{
	char *text = (char *) PlatformGetFileContents(filename);
	return text;
}

//
// Memory
//

internal void *
PlatformAlloc(u32 size)
{
	return VirtualAlloc(0, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

//
// Misc
//

internal i64
PlatformGetTime ()
{
	LARGE_INTEGER res;
	QueryPerformanceCounter(&res);
	return res.QuadPart;
}

internal r64
PlatformTimeElapsed (i64 a, i64 b, i64 perf_frequency)
{
	r64 res = (1000.0 * (b - a)) /
			  (r64) perf_frequency;
	return res;
}

internal void 
PlatformShowPopupTextWindow(char *text)
{
	MessageBox(NULL, text, "...", MB_ICONEXCLAMATION|MB_OK);
}
