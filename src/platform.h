
internal void *PlatformGetFileContents(const char *filename);
internal char *PlatformGetTextFileContents (const char *filename);

internal void PlatformShowPopupTextWindow(char *text);

internal void *PlatformAlloc(u32 size);

internal i64 PlatformGetTime ();
internal r64 PlatformTimeElapsed (i64 a, i64 b, i64 perf_frequency);