#include <dsound.h>

#define DSOUND_CHANNEL_COUNT 2
#define DSOUND_BUFFER_SECONDS 1
#define DSOUND_BYTES_PER_SAMPLE (DSOUND_CHANNEL_COUNT * sizeof(i16))
#define DSOUND_BUFFER_SIZE (DSOUND_BUFFER_SECONDS * SAMPLES_PER_SEC * DSOUND_BYTES_PER_SAMPLE)
#define DSOUND_LATENCY_SAMPLE_COUNT SAMPLES_PER_SEC

struct DSoundOutput
{
	LPDIRECTSOUNDBUFFER dsound_buffer;
	u32 running_sample_index;
};

internal void 
DSoundFillBuffer(DSoundOutput *dsound_out, DWORD byte_to_lock, DWORD bytes_to_write, GameSoundOutputBuffer *source)
{
	VOID *region1;
	DWORD region1_size;
	VOID *region2;
	DWORD region2_size;
	
	if(SUCCEEDED(dsound_out->dsound_buffer->Lock(byte_to_lock, bytes_to_write,
											   &region1, &region1_size,
											   &region2, &region2_size,
											   0)))
	{
		i16 *dest_sample = (i16*)region1;
		i16 *source_sample = source->samples;
		DWORD region1_sample_count = region1_size/DSOUND_BYTES_PER_SAMPLE;
		for(DWORD sample_index = 0; 
			sample_index < region1_sample_count; 
			++sample_index)
		{
			*dest_sample++ = *(source_sample++);
			*dest_sample++ = *(source_sample++);			
			++dsound_out->running_sample_index;
		}
		
		dest_sample = (i16*)region2;
		DWORD region2_sample_count = region2_size/DSOUND_BYTES_PER_SAMPLE;
		for(DWORD sample_index = 0; sample_index < region2_sample_count; ++sample_index)
		{
			*dest_sample++ = *(source_sample++);
			*dest_sample++ = *(source_sample++);			
			++dsound_out->running_sample_index;
		}

		dsound_out->dsound_buffer->Unlock(region1,region1_size, region2,region2_size);
	}
}

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);
internal void
DSoundInit(HWND window, DSoundOutput *dsound_out, GameSoundOutputBuffer *source)
{
	// Inicializando DirectSound
	HMODULE dsound_lib = LoadLibraryA("dsound.dll");
	direct_sound_create *DirectSoundCreate = (direct_sound_create *)GetProcAddress(dsound_lib, "DirectSoundCreate");
	if(dsound_lib)
	{
		LPDIRECTSOUND DirectSound;
		if(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &DirectSound, 0)))
		{
			WAVEFORMATEX wf = {};
			wf.wFormatTag = WAVE_FORMAT_PCM;
			wf.nChannels = DSOUND_CHANNEL_COUNT;
			wf.nSamplesPerSec = SAMPLES_PER_SEC;
			wf.wBitsPerSample = 16;
			wf.nBlockAlign = (wf.nChannels * wf.wBitsPerSample)/8;
			wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;
			wf.cbSize = 0;
			
			if(SUCCEEDED(DirectSound->SetCooperativeLevel(window, DSSCL_PRIORITY)))
			{
				DSBUFFERDESC bd = {};
				bd.dwSize = sizeof(bd);
				bd.dwFlags = DSBCAPS_PRIMARYBUFFER;				
				LPDIRECTSOUNDBUFFER primary_buffer;
				if(SUCCEEDED(DirectSound->CreateSoundBuffer(&bd, &primary_buffer, 0)))
				{
					if(SUCCEEDED(primary_buffer->SetFormat(&wf)))
					{
						OutputDebugStringA("Primary buffer set.\n");
					}
				}
			}
			
			DSBUFFERDESC bd = {};
			bd.dwSize = sizeof(bd);
			bd.dwFlags = 0;
			bd.dwBufferBytes = DSOUND_BUFFER_SIZE;
			bd.lpwfxFormat = &wf;
			if(SUCCEEDED(DirectSound->CreateSoundBuffer(&bd, &dsound_out->dsound_buffer, 0)))
			{
				OutputDebugStringA("Secondary buffer set.\n");
			}
		}
		else
		{
			MessageBox(NULL, "Direct sound initialization failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
		}
	}

	// Preenchimento inicial do buffer
	//DWORD byte_to_lock = 0;
	//DWORD bytes_to_write = DSOUND_LATENCY_SAMPLE_COUNT*DSOUND_BYTES_PER_SAMPLE;
	//DSoundFillBuffer(dsound_out, 0, DSOUND_BUFFER_SIZE, source);
	
	// Iniciando Reproducao
	dsound_out->dsound_buffer->Play(0,0,DSBPLAY_LOOPING);
}

internal void
DSoundOutputSound(DSoundOutput *dsound_out, GameSoundOutputBuffer *buffer)
{
	DWORD play_cursor;
	DWORD write_cursor;
	if(SUCCEEDED(dsound_out->dsound_buffer->GetCurrentPosition(&play_cursor, &write_cursor)))
	{
		DWORD byte_to_lock = (dsound_out->running_sample_index*DSOUND_BYTES_PER_SAMPLE) % DSOUND_BUFFER_SIZE;
		DWORD target_cursor = (play_cursor + (DSOUND_LATENCY_SAMPLE_COUNT*DSOUND_BYTES_PER_SAMPLE)) % DSOUND_BUFFER_SIZE;
		DWORD bytes_to_write;
		
		if(byte_to_lock > target_cursor)
		{
			bytes_to_write = (DSOUND_BUFFER_SIZE - byte_to_lock) + target_cursor;
		}
		else
		{
			bytes_to_write = target_cursor - byte_to_lock;
		}
		
		MixerOutputSound(buffer);
		DSoundFillBuffer(dsound_out, byte_to_lock, bytes_to_write, buffer);
	}
}
