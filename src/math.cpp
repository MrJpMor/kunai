
#include <math.h>

/*
*/

#define SQRT2 (1.41421356237)
#define PI (3.1415927)
#define RAD_90 (PI*0.5f)
#define RAD_180 PI
#define RAD_360 (2.0f*PI)

//
//  2d Vector
//
struct V2
{
    r32 x,y;
};

V2 operator * (V2 v, r32 s){return{v.x*s, v.y*s};}
V2 operator + (V2 a, V2 b){return{a.x+b.x, a.y+b.y};}
//void operator += (V2 *a, V2 b) { a->x += b.x; a->y += b.y;}

inline V2 v2(r32 x, r32 y) {return {x,y};}
#define V2(x,y) v2(x,y)

internal r32
Magnitude (V2 v)
{
    r32 mag2 = (v.x * v.x) + (v.y * v.y);
    return sqrtf(mag2);
}

internal void
Normalize (V2 *v)
{
    r32 mag2 = (v->x * v->x) + (v->y * v->y);
    if(mag2 > 0.0f)
    {
        r32 one_over_mag = 1.0f / sqrtf(mag2);
        v->x *= one_over_mag;
        v->y *= one_over_mag;
    }
}

//
// 3d Vector
//
//#pragma pack(push, 1)
struct V3
{
    r32 x,y,z;
};
//#pragma pack(pop)


V3 v3(r32 x, r32 y, r32 z) {return {x,y,z};}
#define V3(x,y,z) v3(x,y,z)
#define V2_V3(v,z) v3((v).x, (v).y, z)

V3 operator * (V3 v, r32 s){return{v.x*s, v.y*s, v.z*s};}
V3 operator + (V3 a, V3 b){return{a.x+b.x, a.y+b.y, a.z+b.z};}

internal V3
Cross (V3 a, V3 b)
{
    return { a.y * b.z - a.z * b.y,
             a.z * b.x - a.x * b.z,
             a.x * b.y - a.y * b.x };
}

internal r32
Magnitude (V3 v)
{
    r32 mag2 = (v.x * v.x) + (v.y * v.y) + (v.z * v.z);
    return sqrtf(mag2);
}

internal void
Normalize (V3 *v)
{
    r32 mag2 = (v->x * v->x) + (v->y * v->y) + (v->z * v->z);
    if(mag2 > 0.0f)
    {
        r32 one_ver_mag = 1.0f / sqrtf(mag2);
        v->x *= one_ver_mag;
        v->y *= one_ver_mag;
        v->z *= one_ver_mag;
    }
}

//
// 4x4 Matrix
//
struct Mat4
{
    r32 m11, m12, m13, m14;
    r32 m21, m22, m23, m24;
    r32 m31, m32, m33, m34;
    r32 tx,  ty,  tz,  m44;
}; 
// @Threading: BEWARE!!!!!!!
global_variable Mat4 tmp;
#define Mat4ValueArray(m) {(m).m11, (m).m12, (m).m13, (m).m14, (m).m21, (m).m22, (m).m23, (m).m24,(m).m31, (m).m32, (m).m33, (m).m34,(m).tx,  (m).ty,  (m).tz,  (m).m44}
#define Mat4Identity {1.0f, 0.0f, 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f,  0.0f, 0.0f, 1.0f, 0.0f,  0.0f, 0.0f, 0.0f, 1.0f}

V3 operator * (Mat4 m, V3 v)
{
    return
            {
                    v.x*m.m11 + v.y*m.m21 + v.z*m.m31 + m.tx,
                    v.x*m.m12 + v.y*m.m22 + v.z*m.m32 + m.ty,
                    v.x*m.m13 + v.y*m.m23 + v.z*m.m33 + m.tz
            };
}

Mat4 operator * (Mat4 a, Mat4 b)
{
    return
            {
                    a.m11*b.m11 + a.m12*b.m21 + a.m13*b.m31 + a.m14*b.tx,
                    a.m11*b.m12 + a.m12*b.m22 + a.m13*b.m32 + a.m14*b.ty,
                    a.m11*b.m13 + a.m12*b.m23 + a.m13*b.m33 + a.m14*b.tz,
                    a.m11*b.m14 + a.m12*b.m24 + a.m13*b.m34 + a.m14*b.m44,

                    a.m21*b.m11 + a.m22*b.m21 + a.m23*b.m31 + a.m24*b.tx,
                    a.m21*b.m12 + a.m22*b.m22 + a.m23*b.m32 + a.m24*b.ty,
                    a.m21*b.m13 + a.m22*b.m23 + a.m23*b.m33 + a.m24*b.tz,
                    a.m21*b.m14 + a.m22*b.m24 + a.m23*b.m34 + a.m24*b.m44,

                    a.m31*b.m11 + a.m32*b.m21 + a.m33*b.m31 + a.m34*b.tx,
                    a.m31*b.m12 + a.m32*b.m22 + a.m33*b.m32 + a.m34*b.ty,
                    a.m31*b.m13 + a.m32*b.m23 + a.m33*b.m33 + a.m34*b.tz,
                    a.m31*b.m14 + a.m32*b.m24 + a.m33*b.m34 + a.m34*b.m44,

                    a.tx*b.m11 + a.ty*b.m21 + a.tz*b.m31 + a.m44*b.tx,
                    a.tx*b.m12 + a.ty*b.m22 + a.tz*b.m32 + a.m44*b.ty,
                    a.tx*b.m13 + a.ty*b.m23 + a.tz*b.m33 + a.m44*b.tz,
                    a.tx*b.m14 + a.ty*b.m24 + a.tz*b.m34 + a.m44+b.m44
            };
}

internal void
Identity(Mat4 *m)
{
    m->m11 = 1.0f; m->m12 = 0.0f; m->m13 = 0.0f; m->m14 = 0.0f;
    m->m21 = 0.0f; m->m22 = 1.0f; m->m23 = 0.0f; m->m24 = 0.0f;
    m->m31 = 0.0f; m->m32 = 0.0f; m->m33 = 1.0f; m->m34 = 0.0f;
    m->tx = 0.0f;  m->ty = 0.0f;  m->tz = 0.0f;  m->m44 = 1.0f;
}

inline void
ApplyScale(Mat4 *m, V2 s)
{
    m->m11 = m->m11 * s.x;
    m->m22 = m->m22 * s.y;
}

internal void
ApplyTransforms (Mat4 *m, V3 t, V2 s, r32 a)
{
    Identity(m);
    Identity(&tmp);

/*
*/
    //Dimensionamento
    //Identity(&tmp);
    ApplyScale(m,s);
    //*m = *m * tmp;

    //Rotacao //TODO: Fazer funcionar
    r32 qw = cosf(a*0.5f);
    r32 qz = sinf(a*0.5f);

    r32 ww = 2.0f * qw;
    r32 zz = 2.0f * qz;

    tmp.m11 = 1.0f - zz*qz;
    tmp.m12 = ww*qz;
    tmp.m13 = 0.0f;
    tmp.m14 = 0.0f;
    tmp.m21 = -(ww*qz);
    tmp.m22 = 1.0f - (zz*qz);
    tmp.m23 = 0.0f;
    tmp.m24 = 0.0f;
    tmp.m31 = 0.0f;
    tmp.m32 = 0.0f;
    tmp.m33 = 1.0f;
    tmp.m34 = 0.0f;
    *m = *m * tmp;

    //Translacao
    //Identity(&tmp);
    m->tx = t.x;
    m->ty = t.y;
    m->tz = t.z;
    //*m = *m * tmp;
}


internal void
SetLookAt (Mat4 *m,
             r32 eye_x, r32 eye_y, r32 eye_z,
             r32 center_x, r32 center_y, r32 center_z,
             r32 up_x, r32 up_y, r32 up_z)
{
    r32 fx = center_x - eye_x;
    r32 fy = center_y - eye_y;
    r32 fz = center_z - eye_z;

    r32 rlf = 1.0f / Magnitude(V3(fx, fy, fz));
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;

    r32 sx = fy * up_z - fz * up_y;
    r32 sy = fz * up_x - fx * up_z;
    r32 sz = fx * up_y - fy * up_x;

    r32 rls = 1.0f / Magnitude(V3(sx, sy, sz));
    sx *= rls;
    sy *= rls;
    sz *= rls;

    r32 ux = sy * fz - sz * fy;
    r32 uy = sz * fx - sx * fz;
    r32 uz = sx * fy - sy * fx;

    m->m11 = sx;
    m->m12 = ux;
    m->m13 = -fx;
    m->m14 = 0.0f;

    m->m21 = sy;
    m->m22 = uy;
    m->m23 = -fy;
    m->m24 = 0.0f;

    m->m31 = sz;
    m->m32 = uz;
    m->m33 = -fz;
    m->m34 = 0.0f;

    m->tx = -eye_x;
    m->ty = -eye_y;
    m->tz = -eye_z;
    m->m44 = 1.0f;
}

/*
*/
internal void
SetFrustum (Mat4 *m,
             r32 left, r32 right,
             r32 bottom, r32 top,
             r32 near, r32 far)
{
    r32 r_width  = 1.0f / (right - left);
    r32 r_height = 1.0f / (top - bottom);
    r32 r_depth  = 1.0f / (near - far);
    r32 x = 2.0f * (near * r_width);
    r32 y = 2.0f * (near * r_height);
    r32 a = (right + left) * r_width;
    r32 b = (top + bottom) * r_height;
    r32 c = (far + near) * r_depth;
    r32 d = 2.0f * (far * near * r_depth);

    m->m11 = x;
    m->m22 = y;
    m->m31 = a;
    m->m32 = b;
    m->m33 = c;
    m->tz = d;
    m->m34 = -1.0f;
    m->m12 = 0.0f;
    m->m13 = 0.0f;
    m->m14 = 0.0f;
    m->m21 = 0.0f;
    m->m23 = 0.0f;
    m->m24 = 0.0f;
    m->tx = 0.0f;
    m->ty = 0.0f;
    m->m44 = 0.0f;
}

void
SetPerspectiveProjection (Mat4 *m, r32 near, r32 far, r32 fov_y, r32 ratio)
{
    r32 f = 1.0f/tanf(fov_y * (PI / 360.0f));
    r32 range_reciprocal = 1.0f/(near - far);

    m->m11 = f / ratio;
    m->m12 = 0.0f;
    m->m13 = 0.0f;
    m->m14 = 0.0f;

    m->m21 = 0.0f;
    m->m22 = f;
    m->m23 = 0.0f;
    m->m24 = 0.0f;

    m->m31 = 0.0f;
    m->m32 = 0.0f;
    m->m33 = (far + near) * range_reciprocal;
    m->m34 = -1.0f;

    m->tx = 0.0f;
    m->ty = 0.0f;
    m->tz = 2.0f*far*(near * range_reciprocal);
    m->m44 = 0.0f;
}

internal void
SetOrthographicProjection (Mat4 *m,
                             r32 left, r32 right,
                             r32 bottom, r32 top,
                             r32 near, r32 far)
{
    r32 r_width  = 1.0f / (right - left);
    r32 r_height = 1.0f / (top - bottom);
    r32 r_depth  = 1.0f / (far - near);
    r32 x =  2.0f * (r_width);
    r32 y =  2.0f * (r_height);
    r32 z = -2.0f * (r_depth);
    r32 tx = -(right + left) * r_width;
    r32 ty = -(top + bottom) * r_height;
    r32 tz = -(far + near) * r_depth;

    m->m11 = x;
    m->m22  = y;
    m->m33 = z;
    m->tx = tx;
    m->ty = ty;
    m->tz = tz;
    m->m44 = 1.0f;
    m->m12 = 0.0f;
    m->m13 = 0.0f;
    m->m14 = 0.0f;
    m->m21 = 0.0f;
    m->m23 = 0.0f;
    m->m24 = 0.0f;
    m->m31 = 0.0f;
    m->m32 = 0.0f;
    m->m34 = 0.0f;
}

//
// Curves
//

inline V2
InterpolateCubicBezier2D(V2 p0, V2 p1, V2 p2, V2 p3, r32 t)
{
    r32 one_minus_t = (1.0f - t);
    r32 one_minus_t_squared = one_minus_t*one_minus_t;
    r32 one_minus_t_cubed = one_minus_t_squared*(1.0f - t);
    V2 p = (p0*one_minus_t_cubed) + (p1*(3.0f*t*one_minus_t_squared)) + (p2*(3.0f*t*t*one_minus_t)) + (p3*(t*t*t));
    return p;
}


internal u32
GenerateCubicBezier2D(V2 p0, V2 p1, V2 p2, V2 p3, u32 resolution, V2 *points)
{
    u32 count = 0;
    V2 *p = points;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        *p = InterpolateCubicBezier2D(p0, p1, p2, p3, t);
        ++p;
    }
    return count;
}

internal u32
GenerateCubicBezier2D(V2 p0, V2 p1, V2 p2, V2 p3, u32 resolution, r32 *point_xys)
{
    u32 count = 0;
    r32 *xy = point_xys;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        V2 p = InterpolateCubicBezier2D(p0, p1, p2, p3, t);
        *(xy++) = p.x;
        *(xy++) = p.y;
    }
    return count;
}

inline V3
InterpolateCubicBezier3D(V3 p0, V3 p1, V3 p2, V3 p3, r32 t)
{
    r32 one_minus_t = (1.0f - t);
    r32 one_minus_t_squared = one_minus_t*one_minus_t;
    r32 one_minus_t_cubed = one_minus_t_squared*(1.0f - t);
    V3 p = (p0*one_minus_t_cubed) + (p1*(3.0f*t*one_minus_t_squared)) + (p2*(3.0f*t*t*one_minus_t)) + (p3*(t*t*t));
    return p;
}

internal u32
GenerateCubicBezier3D(V3 p0, V3 p1, V3 p2, V3 p3, u32 resolution, V3 *points)
{
    u32 count = 0;
    V3 *p = points;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        *p = InterpolateCubicBezier3D(p0, p1, p2, p3, t);
        ++p;
    }
    return count;
}

inline V2
InterpolateQuadraticBezier2D(V2 p0, V2 p1, V2 p2, r32 t)
{
    r32 one_minus_t = (1.0f - t);
    r32 one_minus_t_squared = one_minus_t*one_minus_t;
    V2 p = (p0*one_minus_t_squared) + (p1*(2.0f*t*one_minus_t)) + (p2*(t*t));
    return p;
}

internal u32
GenerateQuadraticBezier2D(V2 p0, V2 p1, V2 p2, u32 resolution, V2 *points)
{
    u32 count = 0;
    V2 *p = points;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        *p = InterpolateQuadraticBezier2D(p0, p1, p2, t);
        ++p;
    }
    return count;
}

internal u32
GenerateQuadraticBezier2D(V2 p0, V2 p1, V2 p2, u32 resolution, r32 *point_xys)
{
    u32 count = 0;
    r32 *xy = point_xys;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        V2 p = InterpolateQuadraticBezier2D(p0, p1, p2, t);
        *(xy++) = p.x;
        *(xy++) = p.y;
    }
    return count;
}

inline V3
InterpolateQuadraticBezier3D(V3 p0, V3 p1, V3 p2, r32 t)
{
    r32 one_minus_t = (1.0f - t);
    r32 one_minus_t_squared = one_minus_t*one_minus_t;
    V3 p = (p0*one_minus_t_squared) + (p1*(2.0f*t*one_minus_t)) + (p2*(t*t));
    return p;
}

internal u32
GenerateQuadraticBezier3D(V3 p0, V3 p1, V3 p2, u32 resolution, V3 *points)
{
    u32 count = 0;
    V3 *p = points;
    r32 dt = (1.0f/(r32)resolution);
    for (r32 t = 0.0f; t <= 1.0f; t += dt)
    {
        count++;
        *p = InterpolateQuadraticBezier3D(p0, p1, p2, t);
        ++p;
    }
    return count;
}

//
// Misc
//

inline r32
clamp (r32 min, r32 max, r32 val)
{
    return (val > max ? max : (val < min ? min : val));
}