
#define TARGET_FPS 60
#define TARGET_MS_PER_FRAME (1000.0/(r64)TARGET_FPS)

#define GAME_TITLE "Kunai"

#define GAME_MODE_MENU 0
#define GAME_MODE_PLAY 1

#define INPUT_DOWN (0b10000000000000000000000000000000)
#define INPUT_KEY(key, cur) ((b32) (((cur) & INPUT_DOWN)) | (((key) >> 31)&0b1))
#define IS_DOWN(key) ((b32) ((key) & INPUT_DOWN) == INPUT_DOWN)
#define WAS_DOWN(key) ((b32) ((key)&0b1) == 0b1)
struct Input
{
	r32 dt;

	b32 up;
	b32 down;
	b32 left;
	b32 right;

	b32 cam_up;
	b32 cam_down;
	b32 cam_left;
	b32 cam_right;
	b32 cam_dist;

	b32 reset;
	b32 grid;
};

struct Sound
{
	u32 sample_count;
	u32 channel_count;
	i16 *samples[2];
};

struct LineBuffer
{
	u32 gpu_id; // zero se precisa ser mandado p/ gpu novamente
	r32 *points; // zero se ja esta na gpu
	u32 point_count;
	//u32 render_mode;
	V3 color;
};

#define MAX_LINE_SURFACES 8
struct LineSurface
{
	V2 *points;
	u32 point_count;
	u32 *controls;
	u32 control_count;
	u32 curve_count;

	LineBuffer buffer;
};


#define NINJA_WIDTH 1.0f
#define NINJA_HEIGHT 1.0f
#define NINJA_MASS 1.0f
global_variable V2 NINJA_DIMENSIONS = V2(NINJA_WIDTH,NINJA_HEIGHT);
global_variable u32 NINJA_TEXTURE_ID = 0;

struct Ninja
{
	V2 p; // posicao em u
	V2 v; // velocidade em u/s
	V2 a; // aceleracao em u/s/s

	r32 r; // angulo de rotacao
	r32 t; // torque em a/s
};

struct Camera
{
	V3 p;
	V3 focus;
};

struct GameState
{
	u32 mode;
	b32 initialized;

	Input input;

	Sound ambient_sound;

	LineSurface line_surfaces[MAX_LINE_SURFACES];
	u32 line_surface_count;

	LineBuffer grid_buffer;

	Ninja ninja;

	Camera camera;
};

#include "mixer.cpp"
#include "renderer.cpp"
#include "levels.cpp"

//
// Menu principal
//

internal void
MenuUpdateAndRender()
{
	GameState *game_state = (GameState*)game_memory.permanent_memory.base;
	game_state->mode = GAME_MODE_PLAY;
}

//
// Jogo
//

global_variable b32 SHOW_GRID = true;
internal void
GenerateGrid(LineBuffer *grid_buffer, r32 distance)
{
	r32 grid_res = 0.5f * distance;
	r32 grid_span = 16.0f * distance;
	r32 limit = grid_span/2.0f;
	r32 right_limit = limit;
	r32 left_limit = -limit;
	r32 top_limit = limit;
	r32 bottom_limit = -limit;
	u32 grid_point_count = (u32)grid_span*grid_span;
	u32 grid_point_xys_size = grid_point_count*2;//(2*2*2*grid_span)/grid_res;

	r32 *grid_point_xys = 0;
	if((grid_buffer->point_count == grid_point_count) && grid_buffer->points)
	{
		grid_point_xys = grid_buffer->points;
	}
	else
	{
		grid_point_xys = (r32*)PushStruct(&game_memory.transient_memory, r32, grid_point_xys_size);
	}
	
	u32 xyi = 0;
	for (r32 x = left_limit;
		 x <= right_limit;
		 x += grid_res)
	{
		grid_point_xys[xyi++] = x;
		grid_point_xys[xyi++] = bottom_limit;
		grid_point_xys[xyi++] = x;
		grid_point_xys[xyi++] = top_limit;
	}
	for (r32 y = bottom_limit;
		 y <= top_limit;
		  y += grid_res)
	{
		grid_point_xys[xyi++] = left_limit;
		grid_point_xys[xyi++] = y;
		grid_point_xys[xyi++] = right_limit;
		grid_point_xys[xyi++] = y;
	}
	grid_buffer->color = V3(0.27058823529f,0.35294117647f,0.39215686274f);
	grid_buffer->points = grid_point_xys;
	grid_buffer->point_count = grid_point_count;
	grid_buffer->gpu_id = 0;
}

internal void
GameUpdateAndRender(RenderInfo *info)
{
	GameState *game_state = (GameState*)game_memory.permanent_memory.base;
	if(!game_state->initialized)
	{
		//
		// Game Init
		//

		GameState *game_state = (GameState*)game_memory.permanent_memory.base;
		game_state->ambient_sound = LoadWAV("audio\\rainforest_ambience.wav");

		NINJA_TEXTURE_ID = LoadBMP("images\\ninja_test.bmp");

		game_state->camera.p = V3(0.0f, 0.0f, 1.0f);
		game_state->camera.focus = V3(0.0f, 0.0f, -1.0f);

		// Line Surfaces
		GenerateLineSurfaces(game_state);

		// Grid
		GenerateGrid(&game_state->grid_buffer, 1.0f);

		info->clear_color = V3(0.14901960784f, 0.19607843137f, 0.21960784313f);

		game_state->initialized = true;
	}

	//
	// Update e Render
	//

	Input *input = &game_state->input;
	r32 dt = input->dt;

	if(IS_DOWN(input->cam_down))
	{
		int i = 0;
	}
	else if(WAS_DOWN(input->cam_down))
	{
		int i = 0;
	}

	// Camera
	Camera *camera = &game_state->camera;

	V3 cam_move = V3(((IS_DOWN(input->cam_right) ? 1.0f : 0.0f) - (IS_DOWN(input->cam_left) ? 1.0f : 0.0f)), 
					 (((IS_DOWN(input->cam_up) && !IS_DOWN(input->cam_dist)) ? 1.0f : 0.0f) - ((IS_DOWN(input->cam_down) && !IS_DOWN(input->cam_dist)) ? 1.0f : 0.0f)),
					 0.0f);
	Normalize(&cam_move);
	cam_move.z = (((IS_DOWN(input->cam_dist) && IS_DOWN(input->cam_down)) ? 1.0f : 0.0f) - ((IS_DOWN(input->cam_dist) && IS_DOWN(input->cam_up)) ? 1.0f : 0.0f));
	cam_move = cam_move * 10.0f * dt;
	
	camera->p = camera->p + cam_move;
	camera->p.z = clamp(FOREGROUND_Z, 100.0f, camera->p.z);
	camera->focus = camera->p;
	camera->focus.z = BACKGROUND_Z;

	BeginRendering(info);

	Mat4 model_matrix = Mat4Identity;
	
	Mat4 view_matrix = Mat4Identity;
	SetLookAt (&view_matrix, camera->p.x, camera->p.y, camera->p.z,  
							 camera->focus.x, camera->focus.y, camera->focus.z,
							 0.0f, 1.0f, 0.0f);
	
	Mat4 projection_matrix = Mat4Identity;
	r32 ratio = (r32)info->window_width / (r32)info->window_height;
	//SetOrthographicProjection (&projection_matrix, -ratio, ratio, -1.0f, 1.0f, 0.01f, 100.0f);
	//SetFrustum (&projection_matrix, -ratio, ratio, -1.0f, 1.0f, 0.1f, 1.0f);
	SetPerspectiveProjection (&projection_matrix, 0.01f, 100.0f, 90.0f, ratio);
	
	Mat4 mvp_matrix = Mat4Identity;

	// Ninja
	Ninja *ninja = &game_state->ninja;
	r32 damp = 0.8f;
	V2 forces = V2(((IS_DOWN(input->right) ? 1.0f : 0.0f) - (IS_DOWN(input->left) ? 1.0f : 0.0f)),
				   ((IS_DOWN(input->up) ? 1.0f : 0.0f) - (IS_DOWN(input->down) ? 1.0f : 0.0f)));
	Normalize(&forces);
	forces = forces * 1.0f;
	ninja->a = ninja->a + (forces * (1.0f/NINJA_MASS));
	ninja->a = ninja->a * damp * dt;
	ninja->v = ninja->v + (ninja->a * dt);
	ninja->p = ninja->p + ninja->v;

	Rect rect = {};
	rect.fill = true;
	rect.outline = false;
	rect.texture_id = NINJA_TEXTURE_ID;
	ApplyTransforms(&model_matrix, V2_V3(ninja->p, FOREGROUND_Z), NINJA_DIMENSIONS, ninja->r);
	mvp_matrix = (model_matrix * view_matrix) * projection_matrix;
	RenderRect(mvp_matrix, rect); // ninja bmp
	rect.fill = false; 
	rect.outline = true;
	rect.texture_id = 0;
	ApplyTransforms(&model_matrix, V2_V3(ninja->p, FOREGROUND_Z), NINJA_DIMENSIONS, ninja->r);
	mvp_matrix = (model_matrix * view_matrix) * projection_matrix;
	RenderRect(mvp_matrix, rect); // ninja outline

	//Line Surfaces
	ApplyTransforms(&model_matrix, V3(0.0f,0.0f, GROUND_Z), V2(1.0f,1.0f), 0.0f);
	mvp_matrix = (model_matrix * view_matrix) * projection_matrix;
	for (u32 i = 0; i < game_state->line_surface_count; i++)
	{
		LineSurface *ls = &game_state->line_surfaces[i];
		// colisoes
		RenderLineStrip(mvp_matrix, &ls->buffer);
	}

	// Grid
	if(IS_DOWN(input->grid) && !WAS_DOWN(input->grid)) SHOW_GRID = !SHOW_GRID;
	if(SHOW_GRID)
	{
		ApplyTransforms(&model_matrix, V3(0.0f,0.0f, GRID_Z), V2(1.0f,1.0f), 0.0f);
		mvp_matrix = (model_matrix * view_matrix) * projection_matrix;
		//if(((u32)camera->p.z)%2 == 0) GenerateGrid(&game_state->grid_buffer, camera->p.z);
		RenderLines(mvp_matrix, &game_state->grid_buffer);
	}

}