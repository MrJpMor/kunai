@echo off

mkdir build
pushd build

del "*.exe"

..\tools\ctime -begin kunai_build_timings.ctm

cl -Zi ..\code\win32_kunai.cpp user32.lib gdi32.lib opengl32.lib glu32.lib winmm.lib

..\tools\ctime -end kunai_build_timings.ctm
popd

