#ifndef NINJA_KUNAI_H

struct SurfacePiece;
struct CollisionPoint
{
	V2 p;
	V2 n;

	SurfacePiece *sp;
};

enum
{
	NMS_FALLING,
	NMS_GLIDING,

	NMS_STANDING,
	NMS_RUNNING,
	NMS_JUMPING,

	NMS_WALLRUNNING,
	NMS_WALLSTANDING,
	NMS_WALLJUMPING
};
struct NinjaMovementState
{
	u16 last;
	u16 current;
	r32 time_in_current;
};

struct Ninja
{
	V2 pos;
	V2 vel;
	r32 rot;

	V2 forces;
	r32 friction;
	
	V2 gravity;

	CollisionPoint *colpoints;
	u32 ncolpoints;

	CollisionPoint movement_base;
	V2 movement_direction;
	r32 wallrun_time;

	V2 jump_direction;
	u32 jump_count;

	NinjaMovementState movement_state;
};

#define NINJA_KUNAI_H 1
#endif