#ifndef LEVEL_SURFACE_KUNAI_H

/*
sign de pecas triangulares:

(1.0,-1.0f)
****
* *
*

(-1.0,-1.0f)
****
 * *
   *

(-1.0, 1.0f)
   *
 * *
****

(1.0, 1.0f)
*
* *
****

*/

enum
{
	SP_TYPE_TRI = 1,
	SP_TYPE_RECT,
	SP_TYPE_CIRCLE,
	SP_TYPE_CONVEX,
	SP_TYPE_CONCAVE,
};

struct SurfacePiece
{
	u32 type;

	V2 p;
	V2 w;

	V2 sign;
};

struct LivePiece
{
	SurfacePiece *piece;
	void (*update)(LivePiece*, r32);

	V2 vel;
};

struct LevelSurface
{
	SurfacePiece *pieces;
	u32 piece_count;

	LivePiece live_pieces[10];
};

#define LEVEL_SURFACE_KUNAI_H 1
#endif