#include <windows.h>
#include <dsound.h>
#include <assert.h>
#include <math.h>

#include <gl/GLU.h>
#include <gl/gl.h>

#include "kunai.cpp"

#include "win32_config_window_kunai.cpp"

struct Win32
{
	Input input;

	b32 running;
	b32 window_open;
	b32 window_active;
	LPDIRECTSOUNDBUFFER secondary_buffer;
	
	u32 window_width;
	u32 window_height;
	r32 window_ratio;
} *win32;

#include "win32_sound_kunai.cpp"

LRESULT CALLBACK 
WindowProcedure(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch(msg)
    {
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			u32 vkcode = wparam;
			bool isdown = ((lparam & (1<<31)) == 0);
			bool wasdown = ((lparam & (1<<30)) != 0);
			if(wasdown != isdown)
			{
				switch(vkcode)
				{
					case VK_UP:
						win32->input.in.last = win32->input.in.current;
						win32->input.in.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;
					case VK_DOWN:
						win32->input.out.last = win32->input.out.current;
						win32->input.out.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;
						
					case VK_SPACE:
						win32->input.jump.last = win32->input.jump.current;
						win32->input.jump.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;
					case 'W': 
						win32->input.up.last = win32->input.up.current;
						win32->input.up.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);		
					break;
					case 'S':
						win32->input.down.last = win32->input.down.current;
						win32->input.down.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;
					case 'A':
						win32->input.left.last = win32->input.left.current;
						win32->input.left.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;
					case 'D':
						win32->input.right.last = win32->input.right.current;
						win32->input.right.current = (isdown ? INPUT_STATE_DOWN : INPUT_STATE_UP);
					break;

					case 'R':
						if(wasdown) win32->input.reset = true;
					break;

					case VK_F4:
						bool alt_down = ((lparam & (1<<29)) != 0);
						if(alt_down) win32->running = false;
					break;
				}
			}
		}break;
		
		// case WM_MOUSEWHEEL:
		// {	i32 zdelta = GET_WHEEL_DELTA_WPARAM(wparam);
			// if(zdelta > 0)
			// {
				// win32->input.in = true;
			// }
			// else if(zdelta < 0)
			// {
				// win32->input.out = true;
			// }
			// else
			// {
				// win32->input.in = win32->input.out = false;
			// }
		// }	break;
		case WM_LBUTTONUP:
			win32->input.hit.last = win32->input.hit.current;
			win32->input.hit.current = INPUT_STATE_DOWN;
		break;
			
		case WM_ACTIVATEAPP:
			if(wparam == TRUE)
			{
				win32->window_active = true;
			}
			else 
				win32->window_active = false;
		break;
		case WM_SIZE:
			if(win32->window_active)
			{				
				RECT rect;
				GetWindowRect(window, &rect);
				win32->window_width = rect.right - rect.left;
				win32->window_height = rect.bottom - rect.top;
				win32->window_ratio = (r32)win32->window_width/(r32)win32->window_height;
			}
			break;
		case WM_CLOSE:
        case WM_DESTROY:
			win32->running = false;
        break;
		
        default: return DefWindowProc(window, msg, wparam, lparam);
    }
    return 0;
}


internal void *
PlatformReadFile (const char *path)
{
	void *res = 0;
	HANDLE file_handle = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

	if(file_handle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER file_size;
		if(GetFileSizeEx(file_handle, &file_size))
		{
			res = VirtualAlloc(0, (u32)file_size.QuadPart, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if(res)
			{
				DWORD bytes_read;
				if(ReadFile(file_handle, res, (u32)file_size.QuadPart, &bytes_read, 0))
				{
					// 
				}
				else
				{
					MessageBox(NULL, "Failed to read file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
					VirtualFree(res, 0, MEM_RELEASE);
				}
			}
			else
			{
				MessageBox(NULL, "Failed to alloc mem for file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
			}
		}

		CloseHandle(file_handle);
	}
	else
	{
		MessageBox(NULL, "Failed to open file!", "Error!", MB_ICONEXCLAMATION|MB_OK);
	}

	return res;
}

internal b32
PlatformWriteFile (const char *path, u32 size, void *memory)
{
	b32 res = false;
	HANDLE file_handle = CreateFileA(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
	if(file_handle != INVALID_HANDLE_VALUE)
	{
		DWORD bytes_written;
		if(WriteFile(file_handle, memory, size, &bytes_written, 0))
		{
			res = (bytes_written == size);
		}
		else
		{
			// Nao escreveu
		}

		CloseHandle(file_handle);
	}
	else
	{
		// Nao criou
	}

	return res;
}

#pragma pack(push, 1)
struct BMPHeader
{
	u16 FileType;     /* File type, always 4D42h ("BM") */
	u32 FileSize;     /* Size of the file in bytes */
	u16 Reserved1;    /* Always 0 */
	u16 Reserved2;    /* Always 0 */
	u32 BitmapOffset; /* Starting position of image data in bytes */
	u32 Size;            /* Size of this header in bytes */
	i32 Width;           /* Image width in pixels */
	i32 Height;          /* Image height in pixels */
	u16 Planes;          /* Number of color planes */
	u16 BitsPerPixel;    /* Number of bits per pixel */
};
#pragma pack(pop)

internal void
ParseBMP(void *file, void *pixels, u32 *width, u32 *height)
{
	if(file)
	{
		BMPHeader header = *((BMPHeader*)file);

		pixels = (void*)((u8*)file + header.BitmapOffset);
		*width = header.Width;
		*height = header.Height;
	}
}

void *
AllocMemory (u32 size)
{
	return VirtualAlloc(0, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

void
DeallocMemory (void *memory)
{
	free(memory);
}

inline LARGE_INTEGER
GetTime ()
{
	LARGE_INTEGER res;
	QueryPerformanceCounter(&res);
	return res;
}

global_variable i64 PERF_FREQUENCY;

inline r32
TimeElapsed (LARGE_INTEGER a, LARGE_INTEGER b)
{
	r32 res = (1000.0f * (b.QuadPart - a.QuadPart)) /
			  (r32) PERF_FREQUENCY;
	return res;
}

internal void
Win32InitOpenGL (HWND window)
{
	HDC hdc = GetDC(window);

	PIXELFORMATDESCRIPTOR desired_pf = {};
	desired_pf.nSize = sizeof(desired_pf);
	desired_pf.nVersion = 1;
	desired_pf.dwFlags = PFD_SUPPORT_OPENGL|PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER;
	desired_pf.cColorBits = 24;
	desired_pf.cAlphaBits = 8;
	desired_pf.iLayerType = PFD_MAIN_PLANE;
	
	i32 suggested_pf_index = ChoosePixelFormat(hdc, &desired_pf);
	PIXELFORMATDESCRIPTOR suggested_pf = {};
	DescribePixelFormat(hdc, suggested_pf_index, sizeof(suggested_pf), &suggested_pf);
	SetPixelFormat(hdc, suggested_pf_index, &suggested_pf);

	HGLRC opengl_rc = wglCreateContext(hdc);
	if(opengl_rc && wglMakeCurrent(hdc, opengl_rc))
	{	
		OpenGL_Init();
	}
	else 
	{
		assert(0);
	}
	
	ReleaseDC(window,hdc);
}

int WINAPI
WinMain (HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd, int cmd_show)
{	
	LARGE_INTEGER perf_frequency;
	QueryPerformanceFrequency(&perf_frequency);
	PERF_FREQUENCY = perf_frequency.QuadPart;

	// Iniciando janela do jogo
	const char szClassName[] = "KunaiWindowClass";
    WNDCLASSEX wc;
    HWND game_window;

    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wc.lpfnWndProc   = WindowProcedure;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = instance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = szClassName;
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }
	
    GameMemory game_memory = {};
    InitializeMemory(&game_memory.permanent_memory, MEGABYTES(100));

	win32 = PushStruct(&game_memory.permanent_memory, Win32, 1);

	win32->window_width = GetSystemMetrics(SM_CXSCREEN)*0.75f;
    win32->window_height =  GetSystemMetrics(SM_CYSCREEN)*0.75f;
	game_window = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        szClassName,
        GAME_TITLE,
        WS_OVERLAPPEDWINDOW|CS_OWNDC,
        CW_USEDEFAULT, CW_USEDEFAULT, 
		win32->window_width, win32->window_height,
        NULL, NULL, instance, NULL);

    if(game_window == NULL)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }

	timeBeginPeriod(1);
	
	win32->window_open = true;
    ShowWindow(game_window, cmd_show);
    HDC hdc = GetDC(game_window);

    // Iniciando janela de config
    HWND config_window;
    WNDCLASSEX wc_c;
    wc_c.cbSize        = sizeof(WNDCLASSEX);
    wc_c.style         = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    wc_c.lpfnWndProc   = ConfigWindowProcedure;
    wc_c.cbClsExtra    = 0;
    wc_c.cbWndExtra    = 0;
    wc_c.hInstance     = instance;
    wc_c.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc_c.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc_c.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc_c.lpszMenuName  = NULL;
    wc_c.lpszClassName = "ConfigWindowClass";
    wc_c.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    if(!RegisterClassEx(&wc_c))
    {
        MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }
	config_window = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        "ConfigWindowClass",
        "Kunai Configs",
        WS_OVERLAPPEDWINDOW|CS_OWNDC,
        CW_USEDEFAULT, CW_USEDEFAULT, 
		512, 768,
        NULL, NULL, instance, NULL);
    if(config_window == NULL)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }
    ShowWindow(config_window, cmd_show);
    	
	GameState *game_state = PushStruct(&game_memory.permanent_memory, GameState, 1);
	game_state->permanent_memory = &game_memory.permanent_memory;
	game_state->game_mode = GAME_MODE_LEVEL;
	
	// Inicializacao do som
	Win32SoundOut sound_out = {};
	Win32InitSound(game_window, &sound_out);	
	
	// Inicializacao do opengl
	Win32InitOpenGL(game_window);

	RenderBuffer *render_buffer = PushStruct(&game_memory.permanent_memory, RenderBuffer, 1);
	InitRenderer(&game_memory, render_buffer);
	
	r32 ms_since_last_s = 0.0f;
	u32 frame_count = 0;
	
	win32->running = true;
	
	LARGE_INTEGER frame_start = GetTime();
	while(win32->running)
    {
		// Polling de mensagens do sistema
		MSG msg;
		BOOL res;
		// if((res = GetMessage(&msg, 0, 0, 0)) > 0)
		while((res = PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	
		if(win32->window_active)
		{
			// Posicao do mouse
			POINT mouse_position;
			if(GetCursorPos(&mouse_position))
			{
				if(ScreenToClient(game_window, &mouse_position))
				{
					win32->input.point = V2(((mouse_position.x/(r32)win32->window_width)*2-1),
											-((mouse_position.y/(r32)win32->window_height)*2-1));
				}
			}
						
			// Renderizando com opengl
			//OpenGL11_Render(render_buffer);
			OpenGL_Render(render_buffer);
			render_buffer->screen_width = win32->window_width;
			render_buffer->screen_height = win32->window_height;

			// Updates
			switch(game_state->game_mode)
			{
				case GAME_MODE_LEVEL:
					if(!game_state->initialized) LevelInit(game_state);
					LevelUpdateAndRender(&win32->input, game_state, render_buffer);
				break;
				case GAME_MODE_TITLE:
					if(!game_state->initialized) TitleInit(game_state);
					TitleUpdateAndRender(&win32->input, game_state, render_buffer);
				break;
			}
			
			// Output de som
			Win32OutputSound(&sound_out);
			
			// Contando frames
			++frame_count;
			LARGE_INTEGER frame_end = GetTime();
			r32 ms_for_frame = TimeElapsed(frame_start,frame_end);
			win32->input.dt = (ms_for_frame/1000.0f);
			
			if(ms_for_frame < TARGET_MS_PER_FRAME)
			{
				DWORD sleepms = TARGET_MS_PER_FRAME - ms_for_frame;
				Sleep(sleepms);
				ms_for_frame = TimeElapsed(frame_start,GetTime());
			}
			
			ms_since_last_s += ms_for_frame;
			if(ms_since_last_s >= 1000.0f)
			{		
				char str[20];
				wsprintf(str, "%s [%d FPS]", GAME_TITLE, frame_count);
				SetWindowText(game_window, str);
				
				ms_since_last_s = 0.0f;
				frame_count = 0;
			}

			SwapBuffers(hdc);
			UpdateWindow(game_window);

			frame_start = frame_end;
		}
		else
		{
			OutputDebugStringA("OFFSCREEN");
		}
    }
    
	ReleaseDC(game_window,hdc);
	
	return 0;
}