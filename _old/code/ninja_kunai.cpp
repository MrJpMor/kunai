#define MAX_COLLISION_POINTS (1<<4)
#define MAX_KUNAI_COUNT 	 (1<<3)

#define GRAVITY 			 	-1000.0f
#define NINJA_MOVE_MAGNITUDE 	50.0f
#define NINJA_GLIDE_MAGNITUDE	300.0f
#define NINJA_JUMP_MAGNITUDE 	(GRAVITY * -1.5f)
#define NINJA_MAX_VEL 		 	(NINJA_MOVE_MAGNITUDE * 1.002f)
#define NINJA_MAX_WALLRUN_TIME  0.6f
#define NINJA_MAX_JUMPS 	 	2
#define NINJA_MAX_JUMP_TIME		0.1f
#define NINJA_JUMP_FALLOFF		1000.0f
#define NINJA_MASS 			 	1.0f
#define NINJA_WIDTH			 	1.0f
#define HALF_NINJA_WIDTH     	(NINJA_WIDTH*0.5f)
#define NINJA_HEIGHT		 	1.0f
#define HALF_NINJA_HEIGHT    	(NINJA_HEIGHT*0.5f)
#define NINJA_RADIUS		 	0.5f

void
InitNinja(Ninja *ninja)
{
	ninja->rot = 0.0f;
	ninja->pos = V2(0.0f, 60.0f);
}

inline void
AddCollisionPoint(Ninja *ninja, CollisionPoint cp)
{
	assert(ninja->ncolpoints < MAX_COLLISION_POINTS);
	*(ninja->colpoints + ninja->ncolpoints++) = cp;
}

inline void
ThrowKunai (GameState *game_state, V2 pos, V2 vel)
{
	if(game_state->num_kunais >= MAX_KUNAI_COUNT) 
	{
		if(game_state->cur_kunai >= MAX_KUNAI_COUNT)
			game_state->cur_kunai = 0;
	}
	else game_state->num_kunais++;

	Kunai k = {};
	k.pos = pos;
	k.vel = vel;
	k.life = 5;
	*(game_state->kunais + game_state->cur_kunai++) = k;
}

inline void
SolveCollision (Ninja *ninja, V2 p, V2 n, SurfacePiece *sp)
{
	V2 v = ninja->vel;
	
	//find component of velocity parallel to collision normal
	r32 dp = (v.x*n.x + v.y*n.y);
	//project velocity onto collision normal
	//nv is normal velocity
	V2 nv = n * dp;
	
	//t is tangent velocity
	V2 t = v - nv;

	//we only want to apply collision response forces if the object is travelling into, and not out of, the collision
	V2 f = {};
	//V2 b = {};
	//if(dp < 0)
	{
		f = t * ninja->friction;	
		//r32 bounce = 0.0f;
		//b = nv * bounce * -1.0f;
	}

	//project object out of collision
	ninja->pos = ninja->pos + p;
	//apply bounce+friction impulses which alter velocity
	ninja->vel = f; //+ b;

	if(sp)
	{
		CollisionPoint cp = {};
		cp.sp = sp;
		cp.p = (ninja->pos) - (n*NINJA_RADIUS);
		cp.n = n;
		AddCollisionPoint(ninja, cp);
	}
}

internal V2
GetMovementVectorOverPoint(V2 move, CollisionPoint cp)
{
	if((move.y > 0.0f) && (move.x != 0.0f))
		move = normalized(V2(move.x, move.y*1.5f));

	V2 e0 = cp.p + cp.n;
	V2 e = {};
	e.x = move.y * (e0.x - cp.p.x) - (-move.x) * (e0.y - cp.p.y) + cp.p.x;
	e.y = (-move.x) * (e0.x - cp.p.x) + move.y * (e0.y - cp.p.y) + cp.p.y;
	e = normalized(e - cp.p);
	return e;
}

inline void
SetNinjaMovementState (NinjaMovementState *nms, u16 state)
{
	if(nms->last != state)
	{
		nms->last = nms->current;
		nms->current = state;
		nms->time_in_current = 0.0f;
	}
}

inline b32
HasChanged(NinjaMovementState nms)
{
	return (nms.current != nms.last);
}

internal void
HandleInputStateBased (Ninja *ninja, Input *input)
{
	b32 was_down_up = WasDown(input->up);
	b32 is_down_up = IsDown(input->up);
	b32 was_down_down = WasDown(input->down);
	b32 is_down_down = IsDown(input->down);
	b32 was_down_left = WasDown(input->left);
	b32 is_down_left = IsDown(input->left);
	b32 was_down_right = WasDown(input->right);
	b32 is_down_right = IsDown(input->right);

	V2 intended_direction = {};
	intended_direction.x = (is_down_right ? 1.0f : 0.0f) - (is_down_left ? 1.0f : 0.0f);
	intended_direction.y = (is_down_up ? 1.0f : 0.0f) - (is_down_down ? 1.0f : 0.0f);
	intended_direction = normalized(intended_direction);

	V2 intended_direction_p = ninja->pos + (intended_direction * NINJA_RADIUS);

	if(ninja->ncolpoints) // Existem superficies em colisao
	{
		CollisionPoint *closest = ninja->colpoints;
		r32 closestd = magnitude_sq(closest->p - intended_direction_p);
		for(CollisionPoint *cp = ninja->colpoints+1;
			cp < (ninja->colpoints + ninja->ncolpoints);
			++cp)
		{
			r32 cpd = magnitude_sq(cp->p - intended_direction_p);
			if(cpd < closestd)
			{
				closest = cp;
				closestd = cpd;
			}
		}
		ninja->movement_base = *closest;

		if(fabs(ninja->movement_base.n.x) == 1.0f)	// Wall
		{
			if(intended_direction.x != 0.0f) // Wall jump
			{
				if(intended_direction.x - ninja->movement_base.n.x != 0.0f) // Diagonal
					intended_direction = normalized(V2(intended_direction.x, intended_direction.y*1.5f));

				SetNinjaMovementState(&ninja->movement_state, NMS_WALLJUMPING);
			}
			else if(intended_direction.y != 0.0f)
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_WALLRUNNING);
			}
			else
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_WALLSTANDING);
			}
		}
		else // Floor ou ceiling
		{
			// TODO: Ceiling grabs?
			if(intended_direction.y != 0.0f) 
			{
				if(intended_direction.y - ninja->movement_base.n.y != 0.0f) // Diagonal
					intended_direction = normalized(V2(intended_direction.x, intended_direction.y*2.5f));

				SetNinjaMovementState(&ninja->movement_state, NMS_JUMPING);
			}
			else if(intended_direction.x != 0.0f)
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_RUNNING);
			}
			else
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_STANDING);
			}

			if(ninja->movement_base.n.x != 0.0f) // Superficie curva
			{
				// Curvar dir de movimento?
				intended_direction = GetMovementVectorOverPoint(intended_direction, ninja->movement_base);
			}
		}

		ninja->ncolpoints = 0;
	}
	else
	{
		// Colidindo no frame anterior?
		// Cosiderar walk base para o ar
		//ninja->movement_base = {};

		r32 id_magnitude = magnitude_sq(intended_direction);
		b32 in_jump = ((ninja->movement_state.current == NMS_JUMPING)||(ninja->movement_state.current == NMS_WALLJUMPING));

		if(in_jump &&
		   (ninja->movement_state.time_in_current < NINJA_MAX_JUMP_TIME) &&
		   (id_magnitude > 0.0f))
		{

		}
		else
		{
			if(id_magnitude > 0.0f)
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_GLIDING);
			}
			else
			{
				SetNinjaMovementState(&ninja->movement_state, NMS_FALLING);
			}
		}
	}

	r32 movement_magnitude = 0.0f;
	ninja->gravity = V2(0.0f, GRAVITY);
	ninja->movement_direction = intended_direction;

	if(HasChanged(ninja->movement_state))
	{
		switch(ninja->movement_state.last)
		{
			case NMS_JUMPING:
			case NMS_WALLJUMPING:
			{
				//ninja->movement_state.time_in_current = 0.0f;
			} break;
		}
	}

	switch(ninja->movement_state.current)
	{
		case NMS_WALLSTANDING:
		{
			ninja->friction = 0.9f;
		} break;
		case NMS_STANDING:
		{
			ninja->wallrun_time = NINJA_MAX_WALLRUN_TIME;
			ninja->friction = 0.9f;
		} break;

		case NMS_FALLING:
		{
			ninja->wallrun_time = NINJA_MAX_WALLRUN_TIME;
		} break;
		case NMS_GLIDING:
		{
			ninja->movement_direction.y = 0.0f;
			movement_magnitude = NINJA_GLIDE_MAGNITUDE;
			ninja->wallrun_time = NINJA_MAX_WALLRUN_TIME;
		} break;

		case NMS_WALLRUNNING:
		{
			if(ninja->movement_direction.y > 0.0f)
			{
				if(ninja->wallrun_time > 0.0f)
				{
					movement_magnitude = -GRAVITY * ninja->wallrun_time;
					ninja->friction = 1.0f;
					ninja->wallrun_time -= input->dt;
				}
			}
			else
			{
				movement_magnitude = NINJA_MOVE_MAGNITUDE;
				ninja->friction = 1.0f;
			}
		} break;
		case NMS_RUNNING:
		{
			movement_magnitude = NINJA_MOVE_MAGNITUDE;
			ninja->friction = 1.0f;
			ninja->wallrun_time = NINJA_MAX_WALLRUN_TIME;
		} break;

		case NMS_WALLJUMPING:
		{
			//if(intended_direction.x - ninja->movement_base.n.x <= 0.0f)
			{
				if(ninja->movement_state.time_in_current < NINJA_MAX_JUMP_TIME)
				{
					movement_magnitude = NINJA_JUMP_MAGNITUDE - (NINJA_JUMP_FALLOFF * input->dt);
				}
			}
			//else SetNinjaMovementState(&ninja->movement_state, NMS_FALLING);
		} break;
		case NMS_JUMPING:
		{
			//if(intended_direction.y - ninja->movement_base.n.y <= 0.0f)
			{
				if(ninja->movement_state.time_in_current < NINJA_MAX_JUMP_TIME)
				{
					movement_magnitude = NINJA_JUMP_MAGNITUDE - (NINJA_JUMP_FALLOFF * input->dt);
				}
			}
			//else SetNinjaMovementState(&ninja->movement_state, NMS_FALLING);

			ninja->wallrun_time = NINJA_MAX_WALLRUN_TIME;
		} break;
		//InvalidDefault;
	}

	if(ninja->movement_state.current < NMS_JUMPING) // Nao esta nas paredes
	{
		ninja->movement_direction.x *= ninja->movement_base.n.y;
	}	
	V2 movement = ninja->movement_direction * movement_magnitude;
	ninja->forces = ninja->forces + movement;
}

void
UpdateAndRenderNinja (GameState *game_state, RenderBuffer *render_buffer, Input *input)
{
	Ninja *ninja = &game_state->ninja;
	AABB level_bounds = game_state->level_bounds;

	// Renderizando o ninja menos escrotamente
	{
		DebugRenderShape ninja_shape = {};
		ninja_shape.texture = NINJA_TEXTURE;
		ninja_shape.p = ninja->pos;
		ninja_shape.r = 0.0f;
		ninja_shape.w = NINJA_WIDTH;
		ninja_shape.h = NINJA_HEIGHT;
		AddDebugRenderShape(render_buffer, ninja_shape);

		/*
		glBegin(GL_LINES);
	    glColor3f(0.2f, 0.2f, 0.2f);
	    //glVertex2f(ninja->pos.x,ninja->pos.y);
	    r32 da = (2.0f*PI)*0.01f;
		V2 p = ninja->pos + V2(NINJA_RADIUS, 0.0f);
		for(r32 a = 0.0f + da; a <= (2*PI)+da; a += da)
		{
			glVertex2f(p.x,p.y);
			p = {(r32)cosf(a), (r32)sinf(a)};
			p = (normalized(p) * NINJA_RADIUS) + ninja->pos;
	        glVertex2f(p.x,p.y);
		}
		glEnd();
		*/

		if(ninja->ncolpoints)
		{
			DebugColorRect movement_base_rect = {};
			movement_base_rect.color = v3(1.0f,0.2f,0.2f);
			movement_base_rect.p = ninja->movement_base.p;
			movement_base_rect.w = 0.15f;
			movement_base_rect.h = 0.15f;
			AddDebugColorRect(render_buffer, movement_base_rect);

			*(render_buffer->line_points + render_buffer->line_point_count++) = ninja->movement_base.p;
			*(render_buffer->line_points + render_buffer->line_point_count++) = ninja->movement_base.p + ninja->movement_direction;
		}

	}

	r32 dt = input->dt;
	ninja->movement_state.time_in_current += dt;

	//HandleInputAbsolute (ninja, input);
	//HandleInputRelative (ninja, input);
	HandleInputStateBased(ninja, input);

	ninja->forces = ninja->forces;

	r32 one_over_mass = 1.0f/NINJA_MASS;

	r32 drag = 0.0f;
	V2 k1, k2, k3, k4;
	
	V2 accel = {};
	V2 new_vel = {};
	V2 new_pos = {};
	V2 final_force = ninja->gravity;
		
	accel = final_force * one_over_mass;
	k1 = accel * dt;
	final_force = (ninja->forces - ((ninja->vel + (k1*0.5f)) * drag));
	accel = final_force * one_over_mass;
	k2 = accel * dt;
	final_force = (ninja->forces - ((ninja->vel + (k2*0.5f)) * drag));
	accel = final_force * one_over_mass;
	k3 = accel * dt;
	final_force = (ninja->forces - ((ninja->vel + k3) * drag));
	accel = final_force * one_over_mass;
	k4 = accel * dt;
	new_vel = ninja->vel + ((k1 + (k2*2.0f) + (k3*2.0f) + k4) * (1.0f/6.0f));

	r32 vel = magnitude_sq(new_vel);
	if(vel > (NINJA_MAX_VEL*NINJA_MAX_VEL))
		new_vel = (new_vel / sqrtf(vel)) * NINJA_MAX_VEL;

	new_pos = ninja->pos + (new_vel * dt);
	
	ninja->pos = new_pos;
	ninja->vel = new_vel;
	ninja->forces = V2(0.0f,0.0f);

	// Colisao contra limites do level
	V2 min = {level_bounds.center.x - level_bounds.size.x, 
			  level_bounds.center.y - level_bounds.size.y};
	V2 max = {level_bounds.center.x + level_bounds.size.x, 
			  level_bounds.center.y + level_bounds.size.y};
	V2 d = min - (ninja->pos - NINJA_RADIUS);
	if(d.x > 0.0f) // em x
	{
		SolveCollision(ninja, V2(d.x, 0.0f), V2(1.0f, 0.0f), 0);
	}
	else
	{
		d.x = (ninja->pos.x + NINJA_RADIUS) - max.x;
		if(d.x > 0.0f)
			SolveCollision(ninja, V2(-d.x, 0.0f), V2(-1.0f, 0.0f), 0);
	}
	if(d.y > 0.0f) // em y
	{
		SolveCollision(ninja, V2(0.0f, d.y), V2(0.0f, 1.0f), 0);
	}
	else
	{
		d.y = (ninja->pos.y + NINJA_RADIUS) - max.y;
		if(d.y > 0.0f)
			SolveCollision(ninja, V2(0.0f, -d.y), V2(0.0f, -1.0f), 0);
	}
	
	if(IsDown(input->hit))
	{
		ThrowKunai(game_state, ninja->pos + normalized(input->point) + V2(0.0f,0.5f), normalized(input->point) * 0.1f);
		input->hit.current = INPUT_STATE_UP;
	}
}

/*
internal void
HandleInputRelative (Ninja *ninja, Input *input)
{
	b32 up = IsDown(input->up);
	b32 down = IsDown(input->down);
	b32 left = IsDown(input->left);
	b32 right = IsDown(input->right);
	b32 jump = IsDown(input->jump);

	V2 move_dir = {};
	move_dir.x = ((right ? 1.0f : 0.0f) - (left ? 1.0f : 0.0f));
	move_dir.y = ((up ? 1.0f : 0.0f) - (down ? 1.0f : 0.0f));
	move_dir = normalized(move_dir);

	V2 move_dir_pos = ninja->pos + (move_dir * NINJA_RADIUS);
	
	if(ninja->ncolpoints)
	{
		CollisionPoint *closest = ninja->colpoints;
		r32 closestd = magnitude_sq(closest->p - move_dir_pos);
		for(CollisionPoint *cp = ninja->colpoints+1;
			cp < (ninja->colpoints + ninja->ncolpoints);
			++cp)
		{
			r32 cpd = magnitude_sq(cp->p - move_dir_pos);
			if(cpd < closestd)
			{
				closest = cp;
				closestd = cpd;
			}
		}

		move_dir = GetMovementVectorOverPoint(move_dir, *closest);
		ninja->movement_base = *closest;
		ninja->movement_direction = move_dir;

		if(closest->n.x != 0.0f)
			ninja->jump_direction = V2(move_dir.x, -move_dir.y);
		else
			ninja->jump_direction = move_dir;

		ninja->jump_count = 0;

		ninja->ncolpoints = 0;
	}
	else
	{
		ninja->movement_base = {};
	}

	if(IsDown(input->jump)) // Carregando salto
	{
		if(ninja->jump_coef < 1.0f)
			ninja->jump_coef += 10000.0f * input->dt;
	}
	else if(IsUp(input->jump) && WasDown(input->jump)) // Salto
	{
		ninja->forces = ninja->forces + (ninja->jump_direction * (NINJA_JUMP * ninja->jump_coef));
		ninja->jump_coef = 0.0f;
		ninja->jump_direction = V2(0.0f, 1.0f);
	}
	else if(IsDown(input->right) || IsDown(input->left)) // Correndo
	{
		ninja->forces = ninja->forces + (ninja->movement_direction * NINJA_MOVE * (ninja->movement_direction.y != 0.0f ? fabs(ninja->movement_direction.y)*100.0f : 1.0f));
	}
	else if(IsDown(input->down))
	{
		ninja->forces = ninja->forces + (ninja->movement_direction * NINJA_MOVE * (ninja->movement_direction.y != 0.0f ? fabs(ninja->movement_direction.y)*100.0f : 1.0f));
	}

	ninja->gravity = V2(0.0f,GRAVITY);

	bool moving_hor = (input->right ^ input->left);
	bool moving_vert = (input->up ^ input->down);

	if(moving_vert || moving_hor)
	{
		ninja->friction = 1.0f;


		if(ninja->movement_base.sp)
		{
			if((fabs(ninja->movement_base.n.x) == 1.0f) || (ninja->movement_base.n.y == 1.0f))
			{
				*gravity = V2(0.0f, 0.0f);
			}
		}
	}
	else
	{
		ninja->friction = 0.95f;
	}
}

internal void
HandleInputAbsolute (Ninja *ninja, Input *input, V2 *gravity)
{
	b32 up = IsDown(input->up);
	b32 down = IsDown(input->down);
	b32 left = IsDown(input->left);
	b32 right = IsDown(input->right);
	b32 wall_running = false;

	V2 move = {};
	move.x = ((right ? 1.0f : 0.0f) - (left ? 1.0f : 0.0f));
	move.y = ((up ? 1.0f : 0.0f) - (down ? 1.0f : 0.0f));
	move = normalized(move);

	V2 movep = ninja->pos + (move * NINJA_RADIUS);
	
	if(ninja->ncolpoints) // Existem superficies em colisao
	{
		// Achando o ponto + proximo da direcao de movimento pretendida
		CollisionPoint *closest = ninja->colpoints;
		r32 closestd = magnitude_sq(closest->p - movep);
		for(CollisionPoint *cp = ninja->colpoints+1;
			cp < (ninja->colpoints + ninja->ncolpoints);
			++cp)
		{
			r32 cpd = magnitude_sq(cp->p - movep);
			if(cpd < closestd)
			{
				closest = cp;
				closestd = cpd;
			}
		}

		if(fabs(closest->n.x) == 1.0f) // Possivel wall run / jump
		{
			wall_running = true;
			// Representacao vetorial?
			// Algo sobre ter que alternar entre as duas representacoes
			// me indica que o processo pode ser melhor integrado...
			if(closest->n.x > 0.0f)
			{
				up = IsDown(input->left);
				down = IsDown(input->right);
				right = IsDown(input->down);
				left = IsDown(input->up);
			}
			else
			{
				up = IsDown(input->right);
				down = IsDown(input->left);
				right = IsDown(input->up);
				left = IsDown(input->down);	
			}
		}
		else
		{
			move = GetMovementVectorOverPoint(move, *closest);
		}

		ninja->movement_base = *closest;
		ninja->jump_count = 0;

		ninja->ncolpoints = 0;
	}
	else
	{
		ninja->movement_base = {};
	}

	ninja->movement_direction = move;
	ninja->jump_direction = move;

	bool moving_hor = (right ^ left);
	bool moving_vert = (up ^ down);
	
	if(up)
	{
		if(ninja->jump_coef > 0.0f)
		{
			ninja->forces = ninja->forces + (ninja->jump_direction * (NINJA_JUMP * ninja->jump_coef));
			ninja->jump_coef -= JUMP_FALLOFF_PER_SEC * input->dt;
		}
	}

	if((up == false) || (ninja->jump_coef <= 0.0f))
	{
		if((ninja->jump_count < MAX_JUMPS))
		{
			++ninja->jump_count;
			ninja->jump_coef = 1.0f;
		}
	}

	if(moving_hor && !up)
	{
		if(((ninja->vel.x > 0.0f) && (move.x < 0.0f)) || 
		   ((ninja->vel.x < 0.0f) && (move.x > 0.0f)))
		{
			move = move * (NINJA_MOVE * 0.08f);	
		}

		r32 move_vel = NINJA_MOVE; //* ((move.y != 0.0f) && (ninja->movement_base.n.x != 0.0f) ? fabs(move.y)*-GRAVITY : 1.0f);

		if(wall_running)
		{
			if(move.y != 0.0f)
			{
				move_vel *= -GRAVITY;
			}
		}

		ninja->forces = ninja->forces + (move * move_vel);
	}

	*gravity = V2(0.0f,GRAVITY);

	if(moving_hor)
	{
		ninja->friction = 1.0f;
	}
	else
	{
		ninja->friction = 0.8f;
	}
}
*/