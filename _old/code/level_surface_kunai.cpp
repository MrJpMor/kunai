
#define MAX_SURFACE_LINE_COUNT  (1<<8)
#define MAX_SURFACE_PIECE_COUNT (1<<10)

#define RENDER_WIREFRAME 1

internal SurfacePiece*
AddSurfacePiece (LevelSurface *level_surface, SurfacePiece sp)
{
	assert(level_surface->piece_count <= MAX_SURFACE_PIECE_COUNT);
	*(level_surface->pieces + level_surface->piece_count++) = sp;
	return (level_surface->pieces + (level_surface->piece_count-1));
}

internal void
GenRoundCornerPlatform (LevelSurface *level_surface, V2 center, r32 width, r32 height, r32 corner_radius)
{
	if(4.0f*corner_radius > width)  width  = 4.0f*corner_radius;
	if(4.0f*corner_radius > height) height = 4.0f*corner_radius;

	SurfacePiece convex = {};
	convex.type = SP_TYPE_CONVEX;
	convex.p = V2(center.x - (width*0.5f) + corner_radius, center.y + (height*0.5f) - corner_radius);
	convex.w = V2(corner_radius,corner_radius);
	convex.sign = V2(-1.0f,1.0f);
	AddSurfacePiece(level_surface, convex);
	convex.p = V2(center.x - (width*0.5f) + corner_radius, center.y - (height*0.5f) + corner_radius);
	convex.sign = V2(-1.0f,-1.0f);
	AddSurfacePiece(level_surface, convex);

	if(4.0f*corner_radius < width)
	{
		SurfacePiece rect = {};
		rect.type = SP_TYPE_RECT;
		rect.p = center;
		rect.w = V2((width*0.5f) - (corner_radius*2.0f), (height*0.5f));
		AddSurfacePiece(level_surface, rect);
	}
	if(4.0f*corner_radius < height)
	{

		SurfacePiece rect = {};
		rect.type = SP_TYPE_RECT;
		rect.p = center;
		rect.w = V2((width*0.5f), (height*0.5f) - (corner_radius*2.0f));
		AddSurfacePiece(level_surface, rect);
	}
	/*
	else
	{
		SurfacePiece circle = {};
		circle.type = SP_TYPE_CIRCLE;
		circle.p = center - V2(width*0.5f, 0.0f);
		circle.w = V2(corner_radius,corner_radius);
		AddSurfacePiece(level_surface, circle);
		circle.p = center + V2(width*0.5f, 0.0f);
		AddSurfacePiece(level_surface, circle);
	}
	*/

	convex.p = V2(center.x + (width*0.5f) - corner_radius, center.y + (height*0.5f) - corner_radius);
	convex.sign = V2(1.0f,1.0f);
	AddSurfacePiece(level_surface, convex);
	convex.p = V2(center.x + (width*0.5f) - corner_radius, center.y - (height*0.5f) + corner_radius);
	convex.sign = V2(1.0f,-1.0f);
	AddSurfacePiece(level_surface, convex);
}

global_variable r32 radius = 3.0f;
global_variable	V2 base = V2(0.0f,0.0f);

global_variable r32 mr_top = radius*11.0f;
global_variable r32 mr_bottom = radius*5.0f;
global_variable r32 cdir = 1.0f;

internal void
UpdateCenterRect(LivePiece *rect, r32 dt)
{
	if(((rect->piece->p.y > base.y + mr_top) && (cdir == 1.0f)) ||
	   ((rect->piece->p.y < base.y + mr_bottom) && (cdir == -1.0f))) cdir *= -1.0f;

	rect->piece->p = rect->piece->p + (rect->vel * cdir * dt);
}

global_variable V2 rrect_top = V2(radius*8.0f, radius*11.5f);
global_variable V2 rrect_bottom = V2(radius*8.0f, -radius*1.5f);
global_variable r32 rdir = 1.0f;

internal void
UpdateRightRect(LivePiece *rect, r32 dt)
{
	if(((rect->piece->p.y > base.y + rrect_top.y) && (rdir == 1.0f)) ||
	   ((rect->piece->p.y < base.y + rrect_bottom.y) && (rdir == -1.0f))) rdir *= -1.0f;

	rect->piece->p = rect->piece->p + (rect->vel * rdir * dt);
}

internal void
UpdateCenterCircle(LivePiece *circle, r32 dt)
{
	if(((circle->piece->p.y > base.y + 60.0f) && (rdir == 1.0f)) ||
	   ((circle->piece->p.y < base.y + 10.0f) && (rdir == -1.0f))) rdir *= -1.0f;

	circle->piece->p = circle->piece->p + (circle->vel * rdir * dt);
}

void
InitLevelSurface (LevelSurface *level_surface)
{
#if 0
	GenRoundCornerPlatform(level_surface, base - V2(0.0f, 2.0f), 200.0f, 4.0f, 0.5f);

	SurfacePiece rect = {};
	rect.type = SP_TYPE_RECT;
	rect.w = V2(radius, radius*8.0f);
	rect.p = base + V2(radius*2.0f, rect.w.y);
	AddSurfacePiece(level_surface, rect);
	SurfacePiece circle = {};
	circle.type = SP_TYPE_CIRCLE;
	circle.p = rect.p + V2(0.0f, rect.w.y);
	circle.w = V2(3.0f,3.0f);
	AddSurfacePiece(level_surface, circle);
	
	rect.p = base - V2(radius*2.0f, -rect.w.y);
	AddSurfacePiece(level_surface, rect);
	circle.p = rect.p + V2(0.0f, rect.w.y);
	AddSurfacePiece(level_surface, circle);

	circle.p = rect.p + V2(rect.w.x*2.0f, rect.w.y + circle.w.y * 2.0f);
	circle.w = V2(2.001f, 2.001f);
	LivePiece lcircle = {};
	lcircle.piece = AddSurfacePiece(level_surface, circle);
	lcircle.update = &UpdateCenterCircle;
	lcircle.vel = V2(0.0f, 6.0f);
	level_surface->live_pieces[0] = lcircle;

	SurfacePiece convex = {};
	convex.type = SP_TYPE_CONVEX;
	convex.p = base - V2(10.0f*radius, -radius*3.0f);
	convex.w = V2(radius*2.0f,radius*2.0f);
	convex.sign = V2(1.0f, 1.0f);
	AddSurfacePiece(level_surface, convex);

	rect.p = convex.p - V2(0.0f, convex.w.y + radius*0.5f);
	rect.w = V2(radius*2.0f, radius*0.5f);
	AddSurfacePiece(level_surface, rect);

	SurfacePiece concave = {};
	concave.type = SP_TYPE_CONCAVE;	
	concave.w = V2(radius*0.5f, radius*0.5f);
	concave.p = rect.p + V2(rect.w.x + concave.w.x, 0.0f);
	concave.sign = V2(1.0f, 1.0f);
	AddSurfacePiece(level_surface, concave);
	concave.p = rect.p + V2(radius*6.5f, 0.0f);
	concave.sign = V2(-1.0f, 1.0f);
	AddSurfacePiece(level_surface, concave);

	GenRoundCornerPlatform(level_surface, base - V2(50.0f, -radius*2.0f), radius*2.0f, radius, radius/4.0f);
#endif

	SurfacePiece convex = {};
	convex.type = SP_TYPE_CONVEX;
	convex.p = base + V2(0.0f, radius*0.8f);
	convex.w = V2(radius,radius);
	convex.sign = V2(1.0f, 1.0f);
	AddSurfacePiece(level_surface, convex);

	convex.p = base + V2(radius*3.0f, radius*0.8f);
	convex.sign = V2(-1.0f, 1.0f);
	AddSurfacePiece(level_surface, convex);

	convex.p = base + V2(-radius*3.0f, radius*0.8f);
	convex.sign = V2(-1.0f, -1.0f);
	AddSurfacePiece(level_surface, convex);

	convex.p = base + V2(radius*6.0f, radius*0.8f);
	convex.sign = V2(1.0f, -1.0f);
	AddSurfacePiece(level_surface, convex);
}

internal void
SolveCircleCollision (Ninja *ninja, SurfacePiece *sp, V2 orientation)
{
	//if(orientation.y != -1.0f)
	//{	
		V2 o = ninja->pos - sp->p;
		r32 len = magnitude(o);
		r32 trad = sqrtf(sp->w.x*sp->w.x);
		r32 pen = (trad + NINJA_RADIUS) - len;
		
		if(pen > 0.0f)
		{
			o = o / len;
			SolveCollision(ninja, (o*pen), o, sp);
		}
	//}
}

internal void
SolveRectangleCollision(Ninja *ninja, SurfacePiece *sp, V2 penetration, V2 orientation)
{
	if(orientation.x == 0.0f)
	{
		if(orientation.y == 0.0f) // Completamente dentro do bloco
		{
			// Encontrar o menor eixo de penetracao para corrigir o posicionamento
			V2 d = {};
			if(penetration.x < penetration.y) // projetando pra fora no eixo x
			{					
				d.x = ninja->pos.x - sp->p.x;
				if(d.x < 0.0f)
					SolveCollision(ninja, V2(-penetration.x, 0.0f), V2(-1.0f, 0.0f), sp);
				else
					SolveCollision(ninja, V2(penetration.x, 0.0f), V2(1.0f, 0.0f), sp);
				//delta == 0?
			}
			else							 // projetando pra fora no eixo y
			{		
				d.y = ninja->pos.y - sp->p.y;
				if(d.y < 0.0f)
					SolveCollision(ninja, V2(0.0f, -penetration.y), v2(0.0f, -1.0f), sp);
				else
					SolveCollision(ninja, V2(0.0f, penetration.y), V2(0.0f, 1.0f), sp);
				//delta == 0?
			}					
		}
		else // Colidindo verticalmente
		{
			SolveCollision(ninja, V2(0.0f, penetration.y*orientation.y), V2(0.0f, orientation.y), sp);
		}
	}
	else if(orientation.y == 0.0f) // Colidindo horizontalmente
	{
		SolveCollision(ninja, V2(penetration.x*orientation.x, 0.0f), V2(orientation.x, 0.0f), sp);
	}
	else // Colidindo diagonalmente (vertices)
	{			
		// Vertice da colisao
		V2 v = {};
		v.x = ninja->pos.x + (orientation.x * sp->w.x);
		v.y = ninja->pos.y + (orientation.y * sp->w.y);
		// Vertice -> centro do circulo do player		
		V2 d = ninja->pos - v;
		
		r32 len = sqrtf(d.x*d.x + d.y*d.y);
		r32 pen = NINJA_RADIUS - len;
		if(pen > 0.0f) // Vertice dentro do circulo
		{
			if(len == 0.0f) // Tocando a borda do circulo, projetando a 45 graus
				d = orientation * (1.0f/SQRT2);
			else 		   // Projetando em d
				d = d * (1.0f/len);
			
			SolveCollision(ninja, (d*pen), d, sp);
		}
	}	
}

internal void
SolveTriangleCollision (Ninja *ninja, SurfacePiece *sp, V2 penetration, V2 orientation)
{
	V2 sign = sp->sign;
	V2 s = sign / SQRT2;

	if(orientation.x == 0.0f)
	{
		if(orientation.y == 0.0f) // Completamente dentro do bloco
		{
			//this gives is the coordinates of the innermost
			//point on the circle, relative to the tile center	
			V2 o = (ninja->pos - (s * NINJA_RADIUS)) - sp->p;

			//if the dotprod of (ox,oy) and (sx,sy) is negative, the innermost point is in the slope
			//and we need to project it out by the magnitude of the projection of (ox,oy) onto (sx,sy)
			r32 dp = (o.x*s.x) + (o.y*s.y);		
			
			if(dp < 0.0f)
			{
				//collision; project delta onto slope and use this as the slope penetration vector
				s = s * -dp; //(sx,sy) is now the penetration vector		
				
				r32 lenp, lenn;

				//find the smallest axial projection vector
				if(penetration.x < penetration.y)
				{					
					//penetration in x is smaller
					lenp = penetration.x;
					penetration.y = 0.0f;
					
					//get sign for projection along x-axis		
					if((ninja->pos.x - sp->p.x) < 0.0f)
					{
						penetration.x *= -1.0f;
					}
				}
				else
				{		
					//penetration in y is smaller
					lenp = penetration.y;
					penetration.x = 0.0f;
					
					//get sign for projection along y-axis		
					if((ninja->pos.y - sp->p.y)< 0)
					{
						penetration.y *= -1.0f;
					}			
				}

				lenn = sqrtf(s.x*s.x + s.y*s.y);
							
				if(lenp < lenn)
				{
					SolveCollision(ninja, penetration, (penetration * (1.0f/lenp)), sp);
				}
				else
				{
					SolveCollision(ninja, s, s, sp);
				}
			}
		}
		else // Colisao vertical
		{
			if((sign.y * orientation.y) < 0.0f)
			{
				//colliding with face/edge
				SolveCollision(ninja, V2(0.0f, penetration.y*orientation.y), V2(0.0f, orientation.y), sp);
			}
			else
			{
				//we could only be colliding vs the slope OR a vertex
				//look at the vector form the closest vert to the circle to decide

				//this gives is the coordinates of the innermost
				//point on the circle, relative to the closest tile vert	
				V2 o = {};
				o.x = ninja->pos.x - (sp->p.x - (sign.x * sp->w.x));
				o.y = ninja->pos.y - (sp->p.y + (orientation.y * sp->w.y));

				//if the component of (ox,oy) parallel to the normal's righthand normal
				//has the same sign as the slope of the slope (the sign of the slope's slope is signx*signy)
				//then we project by the vertex, otherwise by the normal.
				//note that this is simply a VERY tricky/weird method of determining 
				//if the circle is in side the slope/face's voronoi region, or that of the vertex.											  
				r32 perp = (o.x*-s.y) + (o.y*s.x);
				if((perp*sign.x*sign.y) > 0.0f)
				{
					//collide vs. vertex
					r32 len = sqrtf(o.x*o.x + o.y*o.y);
					r32 pen = NINJA_RADIUS - len;
					if(pen > 0.0f)
					{
						//note: if len=0, then perp=0 and we'll never reach here, so don't worry about div-by-0
						o = o * (1.0f/len);

						SolveCollision(ninja, (o*pen), o, sp);
					}					
				}
				else
				{
					//collide vs. slope
					
					//if the component of (ox,oy) parallel to the normal is less than the circle radius, we're
					//penetrating the slope. note that this method of penetration calculation doesn't hold
					//in general (i.e it won't work if the circle is in the slope), but works in this case
					//because we know the circle is in a neighboring cell
					r32 dp = (o.x*s.x) + (o.y*s.y);
					r32 pen = NINJA_RADIUS - fabs(dp);//note: we don't need the abs because we know the dp will be positive, but just in case..
					if(pen > 0.0f)
					{
						//collision; circle out along normal by penetration amount
						SolveCollision(ninja, (s*pen), s, sp);
					}
				}
			}
		}
	}
	else if(orientation.y == 0.0f) // Colisao horizontal
	{
		if((sign.x*orientation.x) < 0.0f)
		{
			//colliding with face/edge
			SolveCollision(ninja, V2(penetration.x*orientation.x, 0.0f), V2(orientation.x, 0.0f), sp);
		}
		else
		{
			//we could only be colliding vs the slope OR a vertex
			//look at the vector form the closest vert to the circle to decide

			//this gives is the coordinates of the innermost
			//point on the circle, relative to the closest tile vert	
			V2 o = {};
			o.x = ninja->pos.x - (sp->p.x + (orientation.x * sp->w.x));
			o.y = ninja->pos.y - (sp->p.y - (sign.y * sp->w.y));

			//if the component of (ox,oy) parallel to the normal's righthand normal
			//has the same sign as the slope of the slope (the sign of the slope's slope is signx*signy)
			//then we project by the normal, otherwise by the vertex.
			//(NOTE: this is the opposite logic of the vertical case;
			// for vertical, if the perp prod and the slope's slope agree, it's outside.
			// for horizontal, if the perp prod and the slope's slope agree, circle is inside.
			//  ..but this is only a property of flahs' coord system (i.e the rules might swap
			// in righthanded systems))
			//note that this is simply a VERY tricky/weird method of determining 
			//if the circle is in side the slope/face's voronio region, or that of the vertex.											  
			r32 perp = (o.x*-s.y) + (o.y*s.x);
			if((perp*sign.x*sign.y) < 0.0f)
			{
				//collide vs. vertex
				r32 len = sqrtf(o.x*o.x + o.y*o.y);
				r32 pen = NINJA_RADIUS - len;
				if(pen > 0.0f)
				{
					//note: if len=0, then perp=0 and we'll never reach here, so don't worry about div-by-0
					o = o / len;

					SolveCollision(ninja, (o*pen), o, sp);
				}					
			}
			else
			{
				//collide vs. slope
				
				//if the component of (ox,oy) parallel to the normal is less than the circle radius, we're
				//penetrating the slope. note that this method of penetration calculation doesn't hold
				//in general (i.e it won't work if the circle is in the slope), but works in this case
				//because we know the circle is in a neighboring cell
				r32 dp = (o.x*s.x) + (o.y*s.y);
				r32 pen = NINJA_RADIUS - fabs(dp);//note: we don't need the abs because we know the dp will be positive, but just in case..
				if(pen > 0.0f)
				{
					//collision; circle out along normal by penetration amount
					SolveCollision(ninja, (s*pen), s, sp);
				}
			}		
		}
	}
	else // Colisao diagonal
	{
		if(((sign.x*orientation.x) + (sign.y*orientation.y)) > 0.0f) 
		{
			//the dotprod of slope normal and cell offset is strictly positive,
			//therefore obj is in the diagonal neighb pointed at by the normal, and
			//it cannot possibly reach/touch/penetrate the slope
		}
		else
		{
			//collide vs. vertex
			//get diag vertex position
			V2 v = {};
			v.x = ninja->pos.x + (orientation.x * sp->w.x);
			v.y = ninja->pos.y + (orientation.y * sp->w.y);
			
			//calc vert->circle vector		
			V2 d = ninja->pos - v;
			
			r32 len = sqrtf(d.x*d.x + d.y*d.y);
			r32 pen = NINJA_RADIUS - len;
			if(pen > 0.0f)
			{
				//vertex is in the circle; project outward
				if(len == 0.0f)
				{
					//project out by 45deg
					d = orientation * (1.0f/SQRT2);
				}
				else
				{
					d = d * (1.0f/len);
				}

				SolveCollision(ninja, (d*pen), d, sp);
			}
		}
	}
}

internal void
SolveConvexCollision(Ninja *ninja, SurfacePiece *sp, V2 penetration, V2 orientation)
{
	V2 sign = sp->sign;

	if(orientation.x == 0.0f)
	{
		if(orientation.y == 0.0f) // Colisao com o bloco
		{
			//(ox,oy) is the vector from the tile-circle to 
			//the circle's center
			V2 o = {};
			o.x = ninja->pos.x - (sp->p.x - (sign.x * sp->w.x));
			o.y = ninja->pos.y - (sp->p.y - (sign.y * sp->w.y));
	
			r32 twid = sp->w.x * 2.0f;
			r32 trad = sqrtf(twid*twid);
			//this gives us the radius of a circle centered on the tile's corner and extending to the opposite edge of the tile;
			//note that this should be precomputed at compile-time since it's constant
			
			r32 len = sqrtf(o.x*o.x + o.y*o.y);
			r32 pen = (trad + NINJA_RADIUS) - len;

			if(pen > 0.0f)
			{
				r32 lenp;

				//find the smallest axial projection vector
				if(penetration.x < penetration.y)
				{					
					//penetration in x is smaller
					lenp = penetration.x;
					penetration.y = 0.0f;
					
					//get sign for projection along x-axis		
					if((ninja->pos.x - sp->p.x) < 0.0f)
					{
						penetration.x *= -1.0f;
					}
				}
				else
				{		
					//penetration in y is smaller
					lenp = penetration.y;
					penetration.x = 0.0f;
					
					//get sign for projection along y-axis		
					if((ninja->pos.y - sp->p.y) < 0.0f)
					{
						penetration.y *= -1.0f;
					}			
				}

				
				if(lenp < pen)
				{
					SolveCollision(ninja, V2(penetration.x, penetration.y), V2(penetration.x/lenp, penetration.y/lenp), sp);
				}
				else
				{
					//note: len should NEVER be == 0, because if it is, 
					//projeciton by an axis shoudl always be shorter, and we should
					//never arrive here
					o = o * (1.0f/len);
					
					SolveCollision(ninja, (o*pen), o, sp);
				}
			}
		}
		else // Colisao vertical
		{
			if((sign.y*orientation.y) < 0.0f)
			{
				//colliding with face/edge
				SolveCollision(ninja, V2(0.0f, penetration.y*orientation.y), V2(0.0f, orientation.y), sp);
			}
			else
			{
				//obj in neighboring cell pointed at by tile normal;
				//we could only be colliding vs the tile-circle surface

				//(ox,oy) is the vector from the tile-circle to 
				//the circle's center
				V2 o = {};
				o.x = ninja->pos.x - (sp->p.x - (sign.x * sp->w.x));
				o.y = ninja->pos.y - (sp->p.y - (sign.y * sp->w.y));
		
				r32 twid = sp->w.x * 2.0f;
				r32 trad = sqrtf(twid*twid);
				//this gives us the radius of a circle centered on the tile's corner and extending to the opposite edge of the tile;
				//note that this should be precomputed at compile-time since it's constant
				
				r32 len = sqrtf(o.x*o.x + o.y*o.y);
				r32 pen = (trad + NINJA_RADIUS) - len;

				if(pen > 0.0f)
				{
					//note: len should NEVER be == 0, because if it is, 
					//obj is not in a neighboring cell!
					 o = o * (1.0f/len);
					
					SolveCollision(ninja, (o*pen), o, sp);
				}
			}
		}
	}
	else if(orientation.y == 0.0f) // Colisao horizontal
	{
		if((sign.x * orientation.x) < 0.0f)
		{
			//colliding with face/edge
			SolveCollision(ninja, V2(penetration.x * orientation.x, 0.0f), V2(orientation.x, 0.0f), sp);
		}
		else
		{
				//obj in neighboring cell pointed at by tile normal;
				//we could only be colliding vs the tile-circle surface

				//(ox,oy) is the vector from the tile-circle to 
				//the circle's center
				V2 o = {};
				o.x = ninja->pos.x - (sp->p.x - (sign.x * sp->w.x));
				o.y = ninja->pos.y - (sp->p.y - (sign.y * sp->w.y));
		
				r32 twid = sp->w.x * 2.0f;
				r32 trad = sqrtf(twid*twid);
				//this gives us the radius of a circle centered on the tile's corner and extending to the opposite edge of the tile;
				//note that this should be precomputed at compile-time since it's constant
				
				r32 len = sqrtf(o.x*o.x + o.y*o.y);
				r32 pen = (trad + NINJA_RADIUS) - len;
		
				if(pen > 0.0f)
				{
					//note: len should NEVER be == 0, because if it is, 
					//obj is not in a neighboring cell!
					o = o * (1.0f/len);

					SolveCollision(ninja, (o*pen), o, sp);
				}	
		}
	}
	else // Colisao diagonal
	{
		if(((sign.x*orientation.x) + (sign.y*orientation.y)) > 0.0f) 
		{
				//obj in diag neighb cell pointed at by tile normal;
				//we could only be colliding vs the tile-circle surface

				//(ox,oy) is the vector from the tile-circle to 
				//the circle's center
				V2 o = {};
				o.x = ninja->pos.x - (sp->p.x - (sign.x * sp->w.x));
				o.y = ninja->pos.y - (sp->p.y - (sign.y * sp->w.y));
		
				r32 twid = sp->w.x * 2.0f;
				r32 trad = sqrtf(twid*twid);
				//this gives us the radius of a circle centered on the tile's corner and extending to the opposite edge of the tile;
				//note that this should be precomputed at compile-time since it's constant
				
				r32 len = sqrtf(o.x*o.x + o.y*o.y);
				r32 pen = (trad + NINJA_RADIUS) - len;
				
				if(pen > 0.0f)
				{
					//note: len should NEVER be == 0, because if it is, 
					//obj is not in a neighboring cell!
					o = o * (1.0f/len);

					SolveCollision(ninja, (o*pen), o, sp);
				}
		}
		else
		{
			//collide vs. vertex
			//get diag vertex position
			V2 v = {};
			v.x = sp->p.x + (orientation.x * sp->w.x);
			v.y = sp->p.y + (orientation.y * sp->w.y);
			
			V2 d = ninja->pos - v;
			//calc vert->circle vector		
			
			r32 len = sqrtf(d.x*d.x + d.y*d.y);
			r32 pen = NINJA_RADIUS - len;
			if(pen > 0.0f)
			{
				//vertex is in the circle; project outward
				if(len == 0.0f)
				{
					//project out by 45deg
					d = orientation * (1.0f/SQRT2);
				}
				else
				{
					d = d * (1.0f/len);
				}

				SolveCollision(ninja, (d*pen), d, sp);
			}	
		}
	}
}

internal void
SolveConcaveCollision(Ninja *ninja, SurfacePiece *sp, V2 penetration, V2 orientation)
{
	V2 sign = sp->sign;

	if(orientation.x == 0.0f)
	{
		if(orientation.y == 0.0f)
		{
			//colliding with current tile
			
			//(ox,oy) is the vector from the circle to 
			//tile-circle's center
			V2 o = {};
			o.x = (sp->p.x + (sign.x * sp->w.x)) - ninja->pos.x;
			o.y = (sp->p.y + (sign.y * sp->w.y)) - ninja->pos.y;
	
			r32 twid = sp->w.x*2.0f;
			r32 trad = sqrtf(twid*twid);
			//this gives us the radius of a circle centered on the tile's corner and extending to the opposite edge of the tile;
			//note that this should be precomputed at compile-time since it's constant
				
			r32 len = sqrtf(o.x*o.x + o.y*o.y);
			r32 pen = (len + NINJA_RADIUS) - trad;

			if(pen > 0.0f)
			{
				r32 lenp;

				//find the smallest axial projection vector
				if(penetration.x < penetration.y)
				{					
					//penetration in x is smaller
					lenp = penetration.x;
					penetration.y = 0.0f;
						
					//get sign for projection along x-axis		
					if((ninja->pos.x - sp->p.x) < 0.0f)
					{
						penetration.x *= -1.0f;
					}
				}
				else
				{		
					//penetration in y is smaller
					lenp = penetration.y;
					penetration.x = 0.0f;
					
					//get sign for projection along y-axis		
					if((ninja->pos.y - sp->p.y) < 0.0f)
					{
						penetration.y *= -1.0f;
					}
				}

					
				if(lenp < pen)
				{
					SolveCollision(ninja, penetration, (penetration/lenp), sp);
				}
				else
				{
					//we can assume that len >0, because if we're here then
					//(len + obj.r) > trad, and since obj.r <= trad
					//len MUST be > 0
					o = o / len;

					SolveCollision(ninja, (o*pen), o, sp);
				}
			}
		}
		else
		{
			//colliding vertically
			if((sign.y*orientation.y) < 0.0f)
			{			
				//colliding with face/edge
				SolveCollision(ninja, V2(0.0f, penetration.y*orientation.y), V2(0.0f, orientation.y), sp);
			}
			else
			{
				//we could only be colliding vs the vertical tip

				//get diag vertex position
				V2 v = {};
				v.x = sp->p.x - (sign.x*sp->w.x);
				v.y = sp->p.y + (orientation.y*sp->w.y);
				
				//calc vert->circle vector		
				V2 d = ninja->pos - v;
				
				r32 len = sqrtf(d.x*d.x + d.y*d.y);
				r32 pen = NINJA_RADIUS - len;
				if(pen > 0.0f)
				{
					//vertex is in the circle; project outward
					if(len == 0.0f)
					{
						//project out vertically
						d.x = 0.0f;
						d.y = orientation.y;
					}
					else
					{
						d = d / len;
					}

					SolveCollision(ninja, (d*pen), d, sp);
				}
			}
		}		
	}
	else if(orientation.y == 0.0f)
	{
		//colliding horizontally
		if((sign.x*orientation.x) < 0.0f)
		{
			//colliding with face/edge
			SolveCollision(ninja, V2(penetration.x*orientation.x,0.0f), V2(orientation.x,0.0f), sp);
		}
		else
		{
			//we could only be colliding vs the horizontal tip

			//get diag vertex position
			V2 v = {};
			v.x = sp->p.x + (orientation.x*sp->w.x);
			v.y = sp->p.y - (sign.y*sp->w.y);
			
			V2 d = ninja->pos - v;
			//calc vert->circle vector		
			
			r32 len = sqrtf(d.x*d.x + d.y*d.y);
			r32 pen = NINJA_RADIUS - len;
			if(pen > 0.0f)
			{
				//vertex is in the circle; project outward
				if(len == 0.0f)
				{
					//project out horizontally
					d.x = orientation.x;
					d.y = 0.0f;
				}
				else
				{
					d = d / len;
				}

				SolveCollision(ninja, (d*pen), d, sp);
			}
		}
	}
	else
	{
		//colliding diagonally
		if(((sign.x*orientation.x) + (sign.y*orientation.y)) > 0.0f) 
		{
			//the dotprod of slope normal and cell offset is strictly positive,
			//therefore obj is in the diagonal neighb pointed at by the normal, and
			//it cannot possibly reach/touch/penetrate the slope
		}
		else
		{
			//collide vs. vertex
			//get diag vertex position
			V2 v = {};
			v.x = sp->p.x + (orientation.x*sp->w.x);
			v.y = sp->p.y + (orientation.y*sp->w.y);
			
			V2 d = ninja->pos - v;
			//calc vert->circle vector		
			
			r32 len = sqrtf(d.x*d.x + d.y*d.y);
			r32 pen = NINJA_RADIUS - len;
			if(pen > 0.0f)
			{
				//vertex is in the circle; project outward
				if(len == 0.0f)
				{
					//project out by 45deg
					d = orientation / SQRT2;
				}
				else
				{
					d = d / len;
				}

				SolveCollision(ninja, (d*pen), d, sp);
			}	
		}
	}
}

internal void
RenderRectangle(V2 center, V2 size)
{
	V2 a = center - size;
	V2 b = center - V2(-size.x, size.y);
	V2 c = center + size;
	V2 d = center + V2(-size.x, size.y);

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(a.x, a.y, Z_POS_FOREGROUND);
	glVertex3f(b.x, b.y, Z_POS_FOREGROUND);
	glVertex3f(c.x, c.y, Z_POS_FOREGROUND);
	glVertex3f(a.x, a.y, Z_POS_FOREGROUND);
	glVertex3f(c.x, c.y, Z_POS_FOREGROUND);
	glVertex3f(d.x, d.y, Z_POS_FOREGROUND);
	glEnd();
}

internal void
RenderRectangleWireframe(V2 center, V2 size)
{
	V2 a = center - size;
	V2 b = center - V2(-size.x, size.y);
	V2 c = center + size;
	V2 d = center + V2(-size.x, size.y);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex2f(a.x, a.y);
	glVertex2f(b.x, b.y);

	glVertex2f(b.x, b.y);
	glVertex2f(c.x, c.y);

	glVertex2f(c.x, c.y);
	glVertex2f(d.x, d.y);

	glVertex2f(d.x, d.y);
	glVertex2f(a.x, a.y);

	glEnd();
}

internal void
RenderTriangle(V2 center, V2 size, V2 sign)
{
	V2 a,b,c;
	if((sign.x > 0.0f) && (sign.y < 0.0f))
	{
		a = center - size;
		b = center - V2(size.x, -size.y);
		c = center + V2(size.x, size.y);
	}
	else if((sign.x < 0.0f) && (sign.y < 0.0f))
	{
		a = center + V2(size.x, -size.y);
		b = center - V2(size.x, -size.y);
		c = center + size;
	}
	else if((sign.x < 0.0f) && (sign.y > 0.0f))
	{
		a = center - size;
		b = center + size;
		c = center + V2(size.x, -size.y);
	}
	else if((sign.x > 0.0f) && (sign.y > 0.0f))
	{
		a = center - size;
		b = center - V2(size.x, -size.y);
		c = center + V2(size.x, -size.y);
	}

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(a.x, a.y, Z_POS_FOREGROUND);
	glVertex3f(b.x, b.y, Z_POS_FOREGROUND);
	glVertex3f(c.x, c.y, Z_POS_FOREGROUND);
	glEnd();
}

internal void
RenderTriangleWireframe(V2 center, V2 size, V2 sign)
{
	V2 a,b,c;
	if((sign.x > 0.0f) && (sign.y < 0.0f))
	{
		a = center - size;
		b = center - V2(size.x, -size.y);
		c = center + V2(size.x, size.y);
	}
	else if((sign.x < 0.0f) && (sign.y < 0.0f))
	{
		a = center + V2(size.x, -size.y);
		b = center - V2(size.x, -size.y);
		c = center + size;
	}
	else if((sign.x < 0.0f) && (sign.y > 0.0f))
	{
		a = center - size;
		b = center + size;
		c = center + V2(size.x, -size.y);
	}
	else if((sign.x > 0.0f) && (sign.y > 0.0f))
	{
		a = center - size;
		b = center - V2(size.x, -size.y);
		c = center + V2(size.x, -size.y);
	}

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	
	glVertex2f(a.x, a.y);
	glVertex2f(b.x, b.y);

	glVertex2f(b.x, b.y);
	glVertex2f(c.x, c.y);

	glVertex2f(c.x, c.y);
	glVertex2f(a.x, a.y);

	glEnd();
}

internal void
RenderConvex(V2 center, V2 size, V2 sign)
{
	r32 a0, a1;
	r32 da = RAD_90 * (1.0f/6.0f);
	V2 corner = center - V2(sign.x*size.x, sign.y*size.y);

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0f, 0.0f, 0.0f);
	if((sign.x > 0.0f) && (sign.y < 0.0f))
	{
		a0 = -RAD_90;
		a1 = 0.0f;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f) && (sign.y < 0.0f))
	{
		a0 = -RAD_180;
		a1 = -RAD_90;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cos(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f) && (sign.y > 0.0f)) // ok
	{
		a0 = RAD_90;
		a1 = RAD_180;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x > 0.0f) && (sign.y > 0.0f)) // ok
	{
		a0 = 0.0f;
		a1 = RAD_90;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	glEnd();
}

internal void
RenderConvexWireframe(V2 center, V2 size, V2 sign)
{
	r32 a0, a1;
	r32 da = (PI*2.0f) * 0.01f;
	V2 corner = center - V2(sign.x*size.x, sign.y*size.y);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	if((sign.x > 0.0f) && (sign.y < 0.0f))
	{
		a0 = 0.0f;
		a1 = -(PI*0.5f) - da;
		glVertex2f(corner.x, corner.y);
		for (r32 a = a0; a >= a1; a -= da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f) && (sign.y < 0.0f))
	{
		//corner = center + V2(size.x, size.y);
		a0 = -PI*0.5f;
		a1 = -PI-da;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a >= a1; a -= da)
		{
			V2 p = V2((r32)cos(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f) && (sign.y > 0.0f))
	{
		//corner = center + V2(size.x, -size.y);
		a0 = PI*0.5f;
		a1 = PI+da;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	else if((sign.x > 0.0f) && (sign.y > 0.0f))
	{
		//corner = center - V2(size.x, size.y);
		a0 = 0.0f;
		a1 = (PI*0.5f) + da;
		glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
		for (r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = (normalized(p) * size.x*2.0f) + corner;
			glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
		}
	}
	glEnd();
}

internal void
RenderConcave(V2 center, V2 size, V2 sign)
{
	r32 da = (PI*2.0f) * 0.01f;
	r32 a0,a1;
	V2 corner = center - V2(sign.x*size.x, sign.y*size.y);

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0f,0.0f,0.0f);
	if((sign.x > 0.0f)&&(sign.y > 0.0f))
	{
		//corner = pp - size;
		glVertex3f(corner.x, corner.y,Z_POS_FOREGROUND);
		corner = center + size;
		a0 = -PI;
		a1 = -(PI*0.5f)+da;
		for(r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = corner + (normalized(p) * size.x*2.0f);
			glVertex3f(p.x,p.y,Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f)&&(sign.y > 0.0f))
	{
		//corner = center + V2(size.x, -size.y);
		glVertex3f(corner.x, corner.y,Z_POS_FOREGROUND);
		corner = center - V2(size.x, -size.y);
		a0 = 0.0f;
		a1 = -(PI*0.5f)-da;
		for(r32 a = a0; a >= a1; a -= da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = corner + (normalized(p) * size.x*2.0f);
			glVertex3f(p.x,p.y,Z_POS_FOREGROUND);
		}
	}
	else if((sign.x < 0.0f)&&(sign.y < 0.0f))
	{
		//corner = center + size;
		glVertex3f(corner.x, corner.y,Z_POS_FOREGROUND);
		corner = center - size;
		a0 = 0.0f;
		a1 = (PI*0.5f)+da;
		for(r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = corner + (normalized(p) * size.x*2.0f);
			glVertex3f(p.x,p.y,Z_POS_FOREGROUND);
		}
	}
	else if((sign.x > 0.0f)&&(sign.y < 0.0f))
	{
		//corner = center - V2(size.x,-size.y);
		glVertex3f(corner.x, corner.y,Z_POS_FOREGROUND);
		corner = center + V2(size.x,-size.y);
		a0 = (PI*0.5f);
		a1 = PI+da;
		for(r32 a = a0; a <= a1; a += da)
		{
			V2 p = V2((r32)cosf(a), (r32)sinf(a));
			p = corner + (normalized(p) * size.x*2.0f);
			glVertex3f(p.x,p.y,Z_POS_FOREGROUND);
		}
	}
	glEnd();
}

internal void
RenderCircle(V2 center, V2 size)
{
	glBegin(GL_TRIANGLE_FAN);
    glColor3f(0.0f, 0.0f, 0.0f);
    glVertex2f(center.x,center.y);
    r32 da = (2.0f*PI)*0.01f;
	for(r32 a = 0.0f; a <= (2*PI)+da; a += da)
	{
		V2 p = V2((r32)cosf(a), (r32)sinf(a));
		p = (normalized(p) * size.x) + center;
        glVertex2f(p.x,p.y);
	}
	glEnd();
}

internal void
RenderCircleWireframe(V2 center, V2 size)
{
	glBegin(GL_LINES);
    glColor3f(0.0f, 0.0f, 0.0f);
    r32 da = (2.0f*PI)*0.01f;
    V2 p = center + V2(size.x,0.0f);
	for(r32 a = 0.0f; a <= (2*PI)+da; a += da)
	{
		glVertex2f(p.x,p.y);
		p = V2((r32)cosf(a), (r32)sinf(a));
		p = (normalized(p) * size.x) + center;
        glVertex2f(p.x,p.y);
	}
	glEnd();
}

void
UpdateAndRenderLevelSurface (Ninja *ninja, LevelSurface *level_surface, r32 dt)
{
	for (int i = 0; i < 10; ++i)
	{
		LivePiece *lp = &level_surface->live_pieces[i];
		if(lp->piece) lp->update(lp, dt);
	}

	for(SurfacePiece *sp = level_surface->pieces;
		sp < (level_surface->pieces + level_surface->piece_count);
		++sp)
	{
		V2 pp = sp->p;
		V2 pw = sp->w;

		#if 1
		switch(sp->type)
		{
			#if RENDER_WIREFRAME
			case SP_TYPE_RECT: RenderRectangleWireframe(pp,pw); break;
			case SP_TYPE_TRI: RenderTriangleWireframe(pp,pw,sp->sign); break;
			case SP_TYPE_CONVEX: RenderConvex(pp,pw,sp->sign); break;
			case SP_TYPE_CONCAVE: RenderConcave(pp,pw,sp->sign); break;
			case SP_TYPE_CIRCLE: RenderCircleWireframe(pp,pw); break;
			#else
			case SP_TYPE_RECT: RenderRectangle(pp,pw); break;
			case SP_TYPE_TRI: RenderTriangle(pp,pw,sp->sign); break;
			case SP_TYPE_CONVEX: RenderConvex(pp,pw,sp->sign); break;
			case SP_TYPE_CONCAVE: RenderConcave(pp,pw,sp->sign); break;
			case SP_TYPE_CIRCLE: RenderCircle(pp,pw); break;
			#endif
			default: break;
		}
		#endif

		if(sp->type) // Testar colisao
		{
			V2 p  = ninja->pos;
			r32 r = NINJA_RADIUS;
			V2 penetration = {};
			V2 orientation = {};
		
			r32 dx = p.x - pp.x; //tile->obj delta
			penetration.x = (pw.x + r) - fabs(dx); //penetration depth in x

			if(penetration.x >= 0.0f)
			{
				r32 dy = p.y - pp.y; //tile->obj delta
				penetration.y = (pw.y + r) - fabs(dy); //pen depth in y
				
				if(penetration.y > 0.0f)
				{
					//object may be colliding with tile
					
					//circulo em relacao ao sp
					if(dx < -pw.x) 	   orientation.x = -1.0f; // left
					else if(pw.x < dx) orientation.x = 1.0f; // right
					if(dy < -pw.y) 	   orientation.y = -1.0f; // top
					else if(pw.y < dy) orientation.y = 1.0f; //bottom

					switch(sp->type)
					{
						case SP_TYPE_CIRCLE:  SolveCircleCollision(ninja, sp, orientation);				    break;
						case SP_TYPE_TRI:	  SolveTriangleCollision(ninja, sp, penetration, orientation);  break;
						case SP_TYPE_RECT:    SolveRectangleCollision(ninja, sp, penetration, orientation); break;
						case SP_TYPE_CONVEX:  SolveConvexCollision(ninja, sp, penetration, orientation);    break;
						case SP_TYPE_CONCAVE: SolveConcaveCollision(ninja, sp, penetration, orientation);   break;
						default: break;
					}
				}
			}
		}
	}
}

/*
inline void
AddPoint (LineSurface *line_surface, V2 p)
{
	assert(line_surface->pcount <= MAX_SURFACE_LINE_COUNT);

	if(line_surface->pcount == 0)
	{
		*(line_surface->p) = p;
		line_surface->pcount++;
	}
	else
	{
		V2 a = {};
		if(line_surface->pcount == 1) 
			a = *line_surface->p;
		else 
			a = *(line_surface->p + line_surface->pcount-1);
		
		*(line_surface->p + line_surface->pcount++) = p;
		V2 ap = normalized(a - p);
		V2 n  = v2(ap.y, -ap.x);
		if(line_surface->pcount == 1) 
			*line_surface->n = n;
		else 
			*(line_surface->n + (line_surface->pcount-2)) = n;
	}
}

void
InitLevelSurface(LineSurface *line_surface)
{
	line_surface->pdens = 1;

	u32 bumps = 5;
	r32 bump_length = 1.0f;
	r32 resolution = (256.0f/(r32)bumps);
	V2 c = v2(0.0f,0.5f);
	for(u32 b = 0; b < bumps; b++)
	{
		if(b%2 == 0)
		{
			r32 t0 = -PI*0.5f;
			r32 t1 = PI*0.5f;
			r32 increment = (2.0f*PI)*(1.0f/resolution);
			for(r32 t = t0; t <= t1; t += increment)
			{
				V2 p = v2( (r32)sinf(t), (r32)cosf(t));
				p = c + (normalized(p) * (bump_length*0.5f));
				AddPoint(line_surface, p);
			}
		}
		else
		{
			r32 t0 = -PI;
			r32 t1 = 0.0f;
			r32 increment = (2.0f*PI)*(1.0f/resolution);
			for(r32 t = t0; t <= t1; t += increment)
			{
				V2 p = v2( (r32)cosf(t), (r32)sinf(t));
				p = c + (normalized(p) * (bump_length*0.5f));
				AddPoint(line_surface, p);
			}
		}
		c = c + v2(bump_length + 0.1f, 0.0f);
	}

	AddPoint(line_surface, c + v2(2.0f, 0.0f));
}

LineSurface
GetLineSurfaceInArea (LineSurface full_line_surface, r32 area_left_x)
{
	LineSurface res = {};
	u32 leftp = 0; // calc
	res.p = full_line_surface.p + leftp;
	u32 leftn = 0; // calc
	res.n = full_line_surface.n + leftn;
	u32 pcount = 0; // calc
	res.pcount = pcount;
	return res;
}
*/