/*
	TO DO's

	RENDERER
		- renderizacao de bitmaps
	LEVELS
		- geracao de levels
		- particionamento espacial para carregamento parcial(entre outras utilidades)
	FISICA
		- colisao via "meshes" basicos
	ARMADILHAS
		- armadilhas
	SOM
		- reproducao de WAV's
*/


#include "platform_kunai.h"

#include "math_kunai.cpp"
#include "memory_kunai.cpp"
#include "ninja_kunai.h"
#include "level_surface_kunai.h"
#include "kunai.h"

#include "debug_data_kunai.cpp"
#include "renderer_kunai.h"
#include "renderer_kunai.cpp"
#include "opengl_renderer_kunai.cpp"
#include "ninja_kunai.cpp"
#include "level_surface_kunai.cpp"

#include "nuklear.h"


internal void
LevelInit(GameState *game_state)
{
	game_state->ninja.colpoints = PushStruct(game_state->permanent_memory, CollisionPoint, MAX_COLLISION_POINTS);
	game_state->kunais = PushStruct(game_state->permanent_memory, Kunai, MAX_KUNAI_COUNT);
	game_state->level_surface.pieces = PushStruct(game_state->permanent_memory, SurfacePiece, MAX_SURFACE_PIECE_COUNT);

	InitLevelSurface(&game_state->level_surface);
	
	AABB level_bounds = {};
	level_bounds.center = V2(0.0f,50.0f);
	level_bounds.size = V2(105.0f, 51.0f);
	game_state->level_bounds = level_bounds;
	
	InitNinja(&game_state->ninja);
	
	game_state->initialized = true;
}

void
UpdateCamera(V2 *pos, V2 *vel, V2 force, r64 dt)
{ 
	r32 drag = 0.1f;
	r32 one_over_mass = 1.0f/0.001f;
	V2 k1, k2, k3, k4;
	
	V2 accel = {};
	V2 new_vel = {};
	V2 new_pos = {};
	V2 final_force = {};
	
	accel = final_force * one_over_mass;
	k1 = accel * dt;
	final_force = (force - ((*vel + (k1*0.5f)) * drag));
	accel = final_force * one_over_mass;
	k2 = accel * dt;
	final_force = (force - ((*vel + (k2*0.5f)) * drag));
	accel = final_force * one_over_mass;
	k3 = accel * dt;
	final_force = (force - ((*vel + k3) * drag));
	accel = final_force * one_over_mass;
	k4 = accel * dt;
	new_vel = *vel + ((k1 + (k2*2.0f) + (k3*2.0f) + k4) * (1.0f/6.0f));
	
	if(magnitude(new_vel) > NINJA_MAX_VEL)
		new_vel = normalized(new_vel) * (NINJA_MAX_VEL * 0.95f);
	new_pos = *pos + (new_vel * dt);
	
	*pos = new_pos;
	*vel = new_vel;
}

#define RENDER_GRID 1

r32 theta = 0.0f;

void 
LevelUpdateAndRender(Input *input, GameState *game_state, RenderBuffer *render_buffer)
{
	if(input->reset)
	{
		LevelInit(game_state);
		input->reset = false;
	}

	ClearRenderBuffer(render_buffer);

	// Input de movimentos do ninja
	Ninja *ninja = &game_state->ninja;

	UpdateAndRenderNinja(game_state, render_buffer, input);

	UpdateAndRenderLevelSurface(ninja, &game_state->level_surface, input->dt);

	// Direcao do mouse
	//AddQuad(render_buffer, input->point + ninja->pos, 0.1f, 0.1f, 0.0f, {1.0f,0.2f,0.2f});
	// Add quad do ninja 
	//AddQuad(render_buffer, ninja->pos, NINJA_WIDTH, NINJA_HEIGHT, ninja->rot, {0.2f,0.2f,0.2f});

	/*
	*/
	DebugRenderShape shape = {};
	shape.texture = KUNAI_TEXTURE;
	for(Kunai *k = game_state->kunais; 
		k < (game_state->kunais + game_state->num_kunais);
		++k)
	{
		k->pos = k->pos + k->vel;
		k->life -= input->dt;

		shape.p = k->pos;
		shape.r = k->life;
		shape.w = KUNAI_WIDTH;
		shape.h = KUNAI_HEIGHT;
		AddDebugRenderShape(render_buffer, shape);
	}

	// Posicionando camera
	V2 cam_to_player = ninja->pos - render_buffer->cam_p;
	UpdateCamera(&render_buffer->cam_p, &game_state->cam_vel, cam_to_player, input->dt);
	//render_buffer->cam_p = ninja->pos;
	render_buffer->cam_d += (IsDown(input->out) ? 0.3f : 0.0f);
	if(render_buffer->cam_d > MIN_CAM_D)
		render_buffer->cam_d -= (IsDown(input->in) ? 0.3f : 0.0f);

	LINE_WIDTH = 1.0f/render_buffer->cam_d;
	POINT_SIZE = 1.0f/render_buffer->cam_d;
	
	// Grid
#if RENDER_GRID
	//glBegin(GL_LINES);
	{
		r32 left = game_state->level_bounds.center.x - game_state->level_bounds.size.x;
		r32 right = game_state->level_bounds.center.x + game_state->level_bounds.size.x;
		r32 top = game_state->level_bounds.center.y + game_state->level_bounds.size.y;
		r32 bottom = game_state->level_bounds.center.y - game_state->level_bounds.size.y;
		glColor3f(0.0f, 0.0f, 1.0f);
		for (r32 y = bottom; y <= top; ++y)
		{
			glVertex2f(left, y);
			glVertex2f(right, y);
		}
		for (r32 x = left; x <= right; ++x)
		{
			glVertex2f(x, top);
			glVertex2f(x, bottom);
		}
	}
	//glEnd();
#endif

}

internal void 
TitleUpdateAndRender(Input *input, GameState *game_state, RenderBuffer *render_buffer)
{
}

internal void
TitleInit(GameState *game_state)
{
	game_state->initialized = true;
}