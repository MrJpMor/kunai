struct Win32SoundOut
{
	u32 samples_per_sec;
	u32 bytes_per_sample;
	u32 buffer_size;
	
	i32 tone_hz;
	i16 tone_volume;
	i32 wave_period;	
	u32 running_sample_index;
	u32 latency_sample_count;
};

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

internal void
Win32InitDirectSound (HWND window, u32 samples_per_sec, u32 buffer_size)
{
	HMODULE dsound_lib = LoadLibraryA("dsound.dll");
	direct_sound_create *DirectSoundCreate = (direct_sound_create *)GetProcAddress(dsound_lib, "DirectSoundCreate");
	if(dsound_lib)
	{
		LPDIRECTSOUND DirectSound;
		if(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &DirectSound, 0)))
		{
			WAVEFORMATEX wf = {};
			wf.wFormatTag = WAVE_FORMAT_PCM;
			wf.nChannels = 2;
			wf.nSamplesPerSec = samples_per_sec;
			wf.wBitsPerSample = 16;
			wf.nBlockAlign = (wf.nChannels * wf.wBitsPerSample)/8;
			wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;
			wf.cbSize = 0;
			
			if(SUCCEEDED(DirectSound->SetCooperativeLevel(window, DSSCL_PRIORITY)))
			{
				DSBUFFERDESC bd = {};
				bd.dwSize = sizeof(bd);
				bd.dwFlags = DSBCAPS_PRIMARYBUFFER;				
				LPDIRECTSOUNDBUFFER PrimaryBuffer;
				if(SUCCEEDED(DirectSound->CreateSoundBuffer(&bd, &PrimaryBuffer, 0)))
				{
					if(SUCCEEDED(PrimaryBuffer->SetFormat(&wf)))
					{
						OutputDebugStringA("Primary buffer set.\n");
					}
				}
			}
			
			DSBUFFERDESC bd = {};
			bd.dwSize = sizeof(bd);
			bd.dwFlags = 0;
			bd.dwBufferBytes = buffer_size;
			bd.lpwfxFormat = &wf;
			if(SUCCEEDED(DirectSound->CreateSoundBuffer(&bd, &win32->secondary_buffer, 0)))
			{
				OutputDebugStringA("Secondary buffer set.\n");
			}
		}
		else
		{
			MessageBox(NULL, "Direct sound initialization failed!", "Error!", MB_ICONEXCLAMATION|MB_OK);
		}
	}
}

void
Win32FillSoundBuffer(Win32SoundOut *sound_out, DWORD byte_to_lock, DWORD bytes_to_write)
{
	VOID *region1;
	DWORD region1_size;
	VOID *region2;
	DWORD region2_size;
	
	if(SUCCEEDED(win32->secondary_buffer->Lock(byte_to_lock, bytes_to_write,
											   &region1, &region1_size,
											   &region2, &region2_size,
											   0)))
	{
		i16 *sample_out = (i16*)region1;
		DWORD region1_sample_count = region1_size/sound_out->bytes_per_sample;
		for(DWORD sample_index = 0; 
			sample_index < region1_sample_count; 
			++sample_index)
		{
			r32 t = 2.0f*PI*((r32)sound_out->running_sample_index/(r32)sound_out->wave_period);
			i16 sample = (i16)(sinf(t) * sound_out->tone_volume);
			
			*sample_out++ = sample;
			*sample_out++ = sample;
			
			++sound_out->running_sample_index;
		}
		
		sample_out = (i16*)region2;
		DWORD region2_sample_count = region2_size/sound_out->bytes_per_sample;
		for(DWORD sample_index = 0; sample_index < region2_sample_count; ++sample_index)
		{
			r32 t = 2.0f*PI*((r32)sound_out->running_sample_index/(r32)sound_out->wave_period);
			i16 sample = (i16)(sinf(t) * sound_out->tone_volume);
			
			*sample_out++ = sample;
			*sample_out++ = sample;
			
			++sound_out->running_sample_index;
		}	
	}
}

void
Win32InitSound(HWND window, Win32SoundOut *sound_out)
{
	sound_out->samples_per_sec = 48000;
	sound_out->bytes_per_sample = sizeof(i16)*2;
	sound_out->buffer_size = sound_out->samples_per_sec*sound_out->bytes_per_sample;
	sound_out->latency_sample_count = sound_out->samples_per_sec;
	sound_out->tone_hz = 1;
	sound_out->wave_period = sound_out->samples_per_sec/sound_out->tone_hz;
	sound_out->tone_volume = 1000;
	Win32InitDirectSound(window, sound_out->samples_per_sec, sound_out->buffer_size);
	Win32FillSoundBuffer(sound_out, 0, sound_out->latency_sample_count*sound_out->bytes_per_sample);
	win32->secondary_buffer->Play(0,0,DSBPLAY_LOOPING);
}

void
Win32OutputSound(Win32SoundOut *sound_out)
{
	DWORD play_cursor;
	DWORD write_cursor;
	if(SUCCEEDED(win32->secondary_buffer->GetCurrentPosition(&play_cursor, &write_cursor)))
	{
		DWORD byte_to_lock = (sound_out->running_sample_index*sound_out->bytes_per_sample)%sound_out->buffer_size;
		DWORD target_cursor = (play_cursor + (sound_out->latency_sample_count*sound_out->bytes_per_sample))%sound_out->buffer_size;
		DWORD bytes_to_write;
		
		if(byte_to_lock > target_cursor)
		{
			bytes_to_write = (sound_out->buffer_size - byte_to_lock) + target_cursor;
		}
		else
		{
			bytes_to_write = target_cursor - byte_to_lock;
		}
		
		Win32FillSoundBuffer(sound_out, byte_to_lock, bytes_to_write);
	}
}