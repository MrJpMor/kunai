#ifndef MATH_KUNAI
#define MATH_KUNAI

#define SQRT2  1.41421356237
#define PI     3.1415927
#define RAD_90  (PI*0.5f)
#define RAD_180 (PI)
#define RAD_360 (2.0f*PI)

struct V2
{
	r32 x,y;
};

V2 operator - (V2 a, V2 b) {return{a.x-b.x,a.y-b.y};}
V2 operator - (V2 a, r32 s) {return{a.x-s,a.y-s};}
V2 operator + (V2 a, V2 b) {return{a.x+b.x,a.y+b.y};}
V2 operator * (V2 a, r32 s) {return{a.x*s,a.y*s};}
V2 operator / (V2 a, r32 s) {return{a.x/s,a.y/s};}
b32 operator == (V2 a, V2 b) {return(a.x==b.x && a.y==b.y);}
V2 v2(r32 x, r32 y) {return{x,y};}
#define V2(x,y) v2(x,y)
V2 normalized(V2 v)
{
	r32 mag2 = (v.x * v.x) + (v.y * v.y);
	if(mag2 > 0.0f)
	{
		r32 one_over_mag = 1.0f / sqrtf(mag2);
		return {v.x*one_over_mag, v.y*one_over_mag};
	}
	return {0.0f,0.0f};
}
inline r32 magnitude(V2 v) { return sqrtf((v.x * v.x) + (v.y * v.y)); }
inline r32 magnitude_sq(V2 v) { return ((v.x * v.x) + (v.y * v.y)); }
inline r32 cross(V2 a, V2 b)
{
	return ((a.x*b.y) - (a.y*b.x));
}

struct V3
{
	union
	{
		r32 x;
		r32 r;
	};
	union
	{
		r32 y;
		r32 g;
	};
	union
	{
		r32 z;
		r32 b;
	};
};
V3 v3(r32 x, r32 y, r32 z) {return{x,y,z};}

union Mat2
{
	struct
	{
		r32 m11, m12;
		r32 m21, m22;
	};
	struct
	{
		r32 m[4];
	};
};

#endif