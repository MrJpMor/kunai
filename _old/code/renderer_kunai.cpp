#define MAX_RENDER_SHAPES (1<<8)
#define MAX_LINE_POINTS (1<<8)
#define MAX_COLOR_RECTS (1<<8)

global_variable u32 QUAD_SHAPE;
global_variable r32 quad_xy[] = {
								-1.0f, -1.0f,
								-1.0f, 1.0f,
								1.0f, 1.0f,

								-1.0f, -1.0f,
								1.0f, 1.0f,
								1.0f, -1.0f
							   };
global_variable r32 quad_uv[] = {
								0.0f, 0.0f,
								0.0f, 1.0f,
								1.0f, 1.0f,

								0.0f, 0.0f,
								1.0f, 1.0f,
								1.0f, 0.0f
							   };

global_variable u32 NINJA_TEXTURE;
global_variable u32 KUNAI_TEXTURE;
global_variable r32 KUNAI_WIDTH;
global_variable r32 KUNAI_HEIGHT;

void
AddDebugRenderShape(RenderBuffer *render_buffer, DebugRenderShape shape)
{
	*(render_buffer->shapes + render_buffer->shape_count) = shape;
	render_buffer->shape_count = (render_buffer->shape_count+1)%MAX_RENDER_SHAPES;
}

void
AddDebugColorRect(RenderBuffer *render_buffer, DebugColorRect rect)
{
	*(render_buffer->rects + render_buffer->rect_count) = rect;
	render_buffer->rect_count = (render_buffer->rect_count+1)%MAX_COLOR_RECTS;
}

void
AddLine(RenderBuffer *render_buffer, V2 a, V2 b)
{
	*(render_buffer->line_points + render_buffer->line_point_count) = a;
	render_buffer->line_point_count = (render_buffer->line_point_count+1)%MAX_LINE_POINTS;
	*(render_buffer->line_points + render_buffer->line_point_count) = b;
	render_buffer->line_point_count = (render_buffer->line_point_count+1)%MAX_LINE_POINTS;
}

void
ClearRenderBuffer(RenderBuffer *render_buffer)
{
	render_buffer->line_point_count = 0;
	render_buffer->shape_count = 0;
	render_buffer->rect_count = 0;
}

void 
InitRenderer(GameMemory *game_memory, RenderBuffer *render_buffer)
{
	render_buffer->shapes = PushStruct(&game_memory->permanent_memory, DebugRenderShape, MAX_RENDER_SHAPES);
	render_buffer->shape_count = 0;

	render_buffer->line_points = PushStruct(&game_memory->permanent_memory, V2, MAX_LINE_POINTS);
	render_buffer->line_point_count = 0;

	render_buffer->rects = PushStruct(&game_memory->permanent_memory, DebugColorRect, MAX_COLOR_RECTS);
	render_buffer->rect_count = 0;

	render_buffer->cam_d = DEFAULT_CAM_D;

	NINJA_TEXTURE = BitmapToGPU("test.bmp");
}