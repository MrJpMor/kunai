#ifndef RENDERER_KUNAI

struct DebugRenderShape
{
	u32 texture;

	V2 p;
	r32 r;
	r32 w,h;
};

struct DebugColorRect
{
	V3 color;

	V2 p;
	r32 r;
	r32 w,h;
};

struct RenderBuffer
{
	u32 screen_width;
	u32 screen_height;
	
	V2 cam_p;
	r32 cam_d;

	DebugRenderShape *shapes;
	u32 shape_count;

	DebugColorRect *rects;
	u32 rect_count;

	V2 *line_points;
	u32 line_point_count;
};

u32 BitmapToGPU(const char *path);

#define RENDERER_KUNAI 1
#endif