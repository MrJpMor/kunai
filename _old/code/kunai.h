#ifndef KUNAI_H

#define GAME_TITLE "Kunai"
#define Z_POS_FOREGROUND   0.5f
#define Z_POS_BACKGROUND_0 1.0f
#define DEFAULT_CAM_D 	   10.0f
#define MIN_CAM_D		   (Z_POS_FOREGROUND+0.01f)

global_variable r32 LINE_WIDTH;
global_variable r32 POINT_SIZE;

enum
{
	GAME_MODE_TITLE = 1,
	GAME_MODE_LEVEL
};

struct Kunai
{
	V2 pos;
	V2 vel;
	i32 life;
};

struct AABB
{
	V2 center;
	V2 size;
};
AABB aabb(V2 center, V2 size) { return {center,size}; }

struct GameState
{
	u32 game_mode;
	b32 initialized;
	
	Ninja ninja;
	
	Kunai *kunais;
	u32 num_kunais;
	u32 cur_kunai;
	
	AABB level_bounds;
	
	//LineSurface line_surface;
	LevelSurface level_surface;

	V2 cam_vel;
	
	Memory *permanent_memory;
};

#define INPUT_STATE_UP   0x1
#define INPUT_STATE_DOWN 0x2
#define IsUp(state)    (state.current == INPUT_STATE_UP ? 1 : 0)
#define WasUp(state)   (state.last == INPUT_STATE_UP ? 1 : 0)
#define IsDown(state)  (state.current == INPUT_STATE_DOWN ? 1 : 0)
#define WasDown(state) (state.last == INPUT_STATE_DOWN ? 1 : 0)
struct InputState
{
	u16 last;
	u16 current;
};
struct Input
{
	r32 dt;
	
	V2 point;
	InputState hit;
	
	InputState up;
	InputState down;
	InputState left;
	InputState right;
	InputState jump;

	InputState in;
	InputState out;

	b32 reset;
};

void ParseBMP(void *file, void *pixels, u32 *width, u32 *height);

#define KUNAI_H 1
#endif	