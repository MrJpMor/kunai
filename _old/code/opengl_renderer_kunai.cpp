u32
BitmapToGPU(const char *path)
{
	void *file = PlatformReadFile(path);
	void *pixels = (void*) &test_bitmap.pixel_data[0];
	//void *pixels = calloc(16, sizeof(u32));
	//*((u32*)pixels+6) = 0xff00ff00ff;
	u32 width = (u32)test_bitmap.width, height = (u32)test_bitmap.height;
	//u32 width = 4, height = 4;
	//ParseBMP(file, pixels, &width, &height);

	u32 res = 0;
	glGenTextures(1, &res);
	glBindTexture(GL_TEXTURE_2D, res);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pixels);
	glEnable(GL_TEXTURE_2D);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//glBindTexture(GL_TEXTURE_2D, 0);
	
	return res;
}
		
internal void
OpenGL_Init()
{
	glEnable(GL_DEPTH_TEST);
	//glEnable (GL_BLEND);
	//glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	//glFrontFace();

/*
	pixels = calloc(16, sizeof(u32));
	*((u32*)pixels) = 0xff00ff00ff;
	*((u32*)pixels+3) = 0xff00ff00ff;
	u32 width = 4, height = 4;
	glGenTextures(1, &NINJA_TEXTURE);
	glBindTexture(GL_TEXTURE_2D, NINJA_TEXTURE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pixels);
	glEnable(GL_TEXTURE_2D);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
*/
}

internal void
OpenGL_Render(RenderBuffer *render_buffer)
{
	glViewport(0,0,render_buffer->screen_width,render_buffer->screen_height);
	glClearColor(0.20392156862f,0.59607843137f,0.85882352941f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLineWidth(LINE_WIDTH);
	glPointSize(POINT_SIZE);

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	for(V2 *p = render_buffer->line_points; 
		p < (render_buffer->line_points + render_buffer->line_point_count);)
	{
		glVertex3f(p->x, (p++)->y, Z_POS_FOREGROUND);
		glVertex3f(p->x, (p++)->y, Z_POS_FOREGROUND);
	}
	glEnd();

	for(DebugRenderShape *s = render_buffer->shapes;
		s < (render_buffer->shapes+render_buffer->shape_count);
		++s)
	{
		glBindTexture(GL_TEXTURE_2D, NINJA_TEXTURE);
		glEnable(GL_TEXTURE_2D);

		glBegin(GL_TRIANGLES);
		
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		
		r32 half_width = s->w/2.0f;
		r32 half_height = s->h/2.0f;
		V2 a = {(s->p.x - half_width), (s->p.y - half_height)};
		V2 b = {(s->p.x + half_width), (s->p.y - half_height)};
		V2 c = {(s->p.x + half_width), (s->p.y + half_height)};
		V2 d = {(s->p.x - half_width), (s->p.y + half_height)};
		V2 p[] = {a,b,c, a,c,d};
		r32 theta = s->r;
		r64 sint = sin(theta);
		r64 cost = cos(theta);
		for(u8 j = 0; j < 6; ++j)
		{ 
			V2 p0 = p[j];
			p[j].x = cost * (p0.x - s->p.x) - sint * (p0.y - s->p.y) + s->p.x;
			p[j].y = sint * (p0.x - s->p.x) + cost * (p0.y - s->p.y) + s->p.y;
		}
		


		glTexCoord2f(0.0f, 0.0f);		
		glVertex2f(p[0].x, p[0].y);
		glTexCoord2f(1.0f, 0.0f);		
		glVertex2f(p[1].x, p[1].y);
		glTexCoord2f(1.0f, 1.0f);		
		glVertex2f(p[2].x, p[2].y);
		
		glTexCoord2f(0.0f, 0.0f);		
		glVertex2f(p[3].x, p[3].y);
		glTexCoord2f(1.0f, 1.0f);		
		glVertex2f(p[4].x, p[4].y);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(p[5].x, p[5].y);
	
		glEnd();
		// glPopMatrix();
	}

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	r32 ratio = (r32)render_buffer->screen_width / (r32)render_buffer->screen_height;
	glOrtho(-ratio*render_buffer->cam_d,ratio*render_buffer->cam_d,
			-render_buffer->cam_d, render_buffer->cam_d,
			0.0,100.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(render_buffer->cam_p.x, render_buffer->cam_p.y, Z_POS_FOREGROUND,
              render_buffer->cam_p.x, render_buffer->cam_p.y, -Z_POS_FOREGROUND,
              0.0f, 1.0f, 0.0f);

	ClearRenderBuffer(render_buffer);
}