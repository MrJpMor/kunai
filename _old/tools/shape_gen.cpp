#include <windows.h>
#include <malloc.h>
#include <math.h>

#include "..\code\platform_kunai.h"
#include "..\code\math_kunai.cpp"

b32
WriteFile (char *path, int size, void *memory)
{
	b32 res = 0;
	HANDLE file_handle = CreateFileA(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
	if(file_handle != INVALID_HANDLE_VALUE)
	{
		DWORD bytes_written;
		if(WriteFile(file_handle, memory, size, &bytes_written, 0))
		{
			res = (bytes_written == size);
		}
		else
		{
			// Nao escreveu
		}

		CloseHandle(file_handle);
	}
	else
	{
		// Nao criou
	}

	return res;
}

void main()
{
	//r32 pos[]

	r32 da = (PI / 2.0f) * 0.01f;
	V2 corner = center - V2(sign.x*size.x, sign.y*size.y);
	r32 a0 = 0.0f;
	r32 a1 = (PI*0.5f) + da;
	//glVertex3f(corner.x, corner.y, Z_POS_FOREGROUND);
	for (r32 a = a0; a <= a1; a += da)
	{
		V2 p = V2((r32)cosf(a), (r32)sinf(a));
		p = (normalized(p) * size.x*2.0f) + corner;
		//glVertex3f(p.x, p.y, Z_POS_FOREGROUND);
	}

	void *mem = calloc(4);
	WriteFile("assets\\test_shape.dsf", 4, mem);
}